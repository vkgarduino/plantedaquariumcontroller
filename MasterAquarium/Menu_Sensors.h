
class UltrasonicSensorMenu : public Menu
{
      //GetHorizontalLineIndex 0 for title
      //GetHorizontalLineIndex 1 for idealDistance setting
      
      UltrasonicSensor * m_Sensor;
      int _idealDistance;//0 to 999 mm
  public: 

     UltrasonicSensorMenu(Menu *iParent, UltrasonicSensor *iSensor, byte iNameIndex):Menu(iNameIndex)
     {
        m_Sensor = iSensor;
        SetHorizontalLineIndex(0);
        SetBlinkingIndex(0);
        Init(iParent,0);
     }
     
     void GoPrevious(){}
     void GoNext(){}

      void SetDefaultBlinking()
      {
         byte index = GetHorizontalLineIndex();
         if(index == 1 )
            SetBlinkingIndex(12);
      }

      void GoUp()
      {
        if(!m_Sensor)
           return;
        _idealDistance = m_Sensor->GetIdealDistanceOfWaterLevelFromTopInMM();
        if(GetHorizontalLineIndex() > 0)
             SetHorizontalLineIndex(GetHorizontalLineIndex()-1);
        SetDefaultBlinking();
      }
      
      void GoDown()
      {
          if(!m_Sensor || GetHorizontalLineIndex() == 1)
             return;
          _idealDistance = m_Sensor->GetIdealDistanceOfWaterLevelFromTopInMM();
          SetHorizontalLineIndex(GetHorizontalLineIndex()+1);
          SetDefaultBlinking();
      }

      void OnOk()
      {
          if(!m_Sensor)
             return;
          if(GetHorizontalLineIndex() == 0)
            GoDown();
          else if(GetBlinkingIndex() == 16 && (GetHorizontalLineIndex() == 1) )
          {
            m_Sensor->SetIdealDistanceOfWaterLevelFromTopInMM(_idealDistance);
          }
          SetDefaultBlinking();
      }



      void Display()
      {
        if(!m_Sensor)
           return;
         InitFirstRow();
         AppendSpecialMessage(m_Sensor->GetNameIndex());
         if(GetHorizontalLineIndex() > 0)
            AppendSpecialMessage(setTextName);

         InitSecondRow();
         if(GetHorizontalLineIndex() == 0)
            AppendSpecialMessage(backOkTextName);
         else if(GetHorizontalLineIndex() ==1)
           PrintSensorValue(idealLevelTxtName ,&_idealDistance);
         
         if( GetHorizontalLineIndex() > 0)
         {
           SetLcdCursor(GetBlinkingIndex() , 1);
           SetLcdBlink(true);
         }
         else 
         {
            SetLcdBlink(false);
         }
      }


     void OnDigitPress(byte iDigit)
     {
        if( GetHorizontalLineIndex() == 1)
           _idealDistance = ReadDigitAndIfCorrectMoveNext(_idealDistance,3/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,100/*max limit reached*/,12/*start index of nb*/,16/*next number location or 16 if no nb*/,true);
     }
};


class TemperatureSensorMenu : public Menu
{
      //GetHorizontalLineIndex 0 for title
      //GetHorizontalLineIndex 1 for temp setting
      
      TemperatureSensor * m_Sensor;
      int _criticalTemperature;//0 to 999 mm
  public: 

     TemperatureSensorMenu(Menu *iParent, TemperatureSensor *iSensor, byte iNameIndex):Menu(iNameIndex)
     {
       _criticalTemperature = 0;
        m_Sensor = iSensor;
        SetHorizontalLineIndex(0);
        SetBlinkingIndex(0);
        Init(iParent,0);
     }
     
     void GoPrevious(){}
     void GoNext(){}

      void SetDefaultBlinking()
      {
         if(GetHorizontalLineIndex() == 1)
            SetBlinkingIndex(11);
      }

      void GoUp()
      {
        if(!m_Sensor)
           return;
        _criticalTemperature = m_Sensor->GetCriticalTemp_InMultipleOf10();
        if(GetHorizontalLineIndex() > 0)
             SetHorizontalLineIndex(GetHorizontalLineIndex()-1);
        SetDefaultBlinking();
      }
      
      void GoDown()
      {
          if(!m_Sensor)
             return;
          if(GetHorizontalLineIndex() == 1)
             return;
          _criticalTemperature = m_Sensor->GetCriticalTemp_InMultipleOf10();
          SetHorizontalLineIndex(GetHorizontalLineIndex()+1);
          SetDefaultBlinking();
      }

      void OnOk()
      {
          if(!m_Sensor)
             return;
          if(GetHorizontalLineIndex() == 0)
          {
            GoDown();
          }
          else if(GetBlinkingIndex() == 16 && (GetHorizontalLineIndex() == 1) )
          {
           m_Sensor->SetCriticalTemp_InMultipleOf10(_criticalTemperature);
           SetDefaultBlinking();
           _criticalTemperature = m_Sensor->GetCriticalTemp_InMultipleOf10();
          }
      }

      void Display()
      {
        if(!m_Sensor)
           return;
         InitFirstRow();
         AppendSpecialMessage(m_Sensor->GetNameIndex());
         if(GetHorizontalLineIndex() > 0)
            AppendSpecialMessage(setTextName);

         InitSecondRow();
         if(GetHorizontalLineIndex() == 0)
            AppendSpecialMessage(backOkTextName);
         else if(GetHorizontalLineIndex() ==1)
           PrintSensorValue(critTempTextName,&_criticalTemperature);
         
         if( GetHorizontalLineIndex() > 0)
         {
           SetLcdCursor(GetBlinkingIndex() , 1);
           SetLcdBlink(true);
         }
         else 
         {
            SetLcdBlink(false);
         }
      }


     void OnDigitPress(byte iDigit)
     {
        if( GetHorizontalLineIndex() != 1)
          return;

         _criticalTemperature = ReadDigitAndIfCorrectMoveNext(_criticalTemperature,3/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,999/*max limit reached*/,11/*start index of nb*/,16/*next number location or 16 if no nb*/,true);
     }
};
