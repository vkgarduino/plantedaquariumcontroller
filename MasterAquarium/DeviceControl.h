


bool IsDeviceSensorBased(IndividualDevice *iDevice)
{
   bool sensorBased = false;
   for(byte j = 0; j< iDevice->GetTotalNbPrograms(); j++)
   {
       if(Circuit_SensorControlled == iDevice->GetProgram(j)->GetProgramOnOffControlType())
          sensorBased = true;
   }    
   return sensorBased;
}


int GetIteratedCurrentTemperatureValue(TemperatureSensor *tempSensor)
{
       int currentTemp = 0;
       byte maxIteration = 3;
       for(byte i = 1; i<=maxIteration; i++)
       {
          currentTemp = currentTemp + tempSensor->GetTemp_InMultipleOf10();
          delay(10);
       }
       currentTemp = currentTemp/maxIteration;
       return currentTemp;
}


status_Sensor_Device IfToSwitchOnTempSensorBasedDevice(int &currentTemp, int &iCriticalTemp, int &deltaTemp, bool ifDeviceSwitchOnIfTempExceedCritical)
{ 
       if(ifDeviceSwitchOnIfTempExceedCritical)
       {
         if(currentTemp > iCriticalTemp)
            return E_ON;
         else if(currentTemp < iCriticalTemp - deltaTemp)
            return E_OFF;       
       }
       else
       {
         if(currentTemp< iCriticalTemp)
            return E_ON;
         else if(currentTemp > iCriticalTemp + deltaTemp)
            return E_OFF;       
       }
       return E_NOCHANGE;
}

int GetIteratedCurrentDepthValue(UltrasonicSensor *depthSensor)
{
  int currentDepth = 0;
  byte maxIteration = 3;
  for(byte i = 1; i<=maxIteration; i++)
  {
     currentDepth = currentDepth + depthSensor->GetWaterDistanceFromTopInMM();
     delay(10);
  }
  currentDepth = currentDepth/maxIteration;
  return currentDepth;
}


status_Sensor_Device IfToSwitchOnDepthSensorBasedDevice(int currentDepth,int &idealDepth, int & deltaDepth)
{ 
  
  if(currentDepth > idealDepth + deltaDepth) //deltaDepth mm more than ideal depth / can be called critical depth
     return E_ON;
  else if(currentDepth < idealDepth)
    return E_OFF;
 return E_NOCHANGE;
}

byte status_waterOut = 0;
void ControlDevices()
{
   delay(20);
   int tempOfAquarium_InMultipleOf10 = GetIteratedCurrentTemperatureValue(&tankTemp);
   int CriticalTempOfAquarium_InMultipleOf10 = tankTemp.GetCriticalTemp_InMultipleOf10();
   int DeltaTempOfAquarium_InMultipleOf10 = tankTemp.GetDeltaTemp_InMultipleOf10();

    int tempOfLight_InMultipleOf10 = GetIteratedCurrentTemperatureValue(&lightTemp);
   int CriticalTempOfLight_InMultipleOf10 = lightTemp.GetCriticalTemp_InMultipleOf10();
   int DeltaTempOfLight_InMultipleOf10 = lightTemp.GetDeltaTemp_InMultipleOf10();

   int depthInAquarium_FromTop_InMM = GetIteratedCurrentDepthValue(&aquariumDepth);
   int IdealdepthInAquarium_FromTop_InMM = aquariumDepth.GetIdealDistanceOfWaterLevelFromTopInMM();
   int deltaDepthInAquarium_InMM = 5;

   int depthInReservoire_FromTop_InMM = GetIteratedCurrentDepthValue(&reservoirDepth);
   int IdealdepthInReservoire_FromTop_InMM = reservoirDepth.GetIdealDistanceOfWaterLevelFromTopInMM();
   int deltaDepthInReservoire_InMM = 10;
   

     
   //devices 
   byte mlQuantityOfMacro = 0,mlQuantityOfMicro = 0;//, nbRoundsOfFood = 0;
   bool specialWaterChangeScenarioConsidered = false;
   for(byte i = 0; i< AVAILABLE_RELAY_DEVICES; i++) 
   {

      bool considerSensorEquivalentBehaviorOfAquariumWaterInDevice = false;
      if(i == Aquarium_WaterOut_Device_Index || i == Aquarium_WaterIn_Device_Index)
      {
         if(_currentWaterchangeState == WATER_OUT || _currentWaterchangeState == WATER_CHANGE || _currentWaterchangeState == WATER_IN)
         {
          
           if(specialWaterChangeScenarioConsidered)
             continue;
           specialWaterChangeScenarioConsidered = true;
           if(depthInAquarium_FromTop_InMM >= aquariumDepth.GetWaterOutDistanceFromTopInMM() && (_currentWaterchangeState == WATER_CHANGE || _currentWaterchangeState == WATER_OUT))
           {
             status_waterOut++;
             if(5 == status_waterOut)
             {
                if(_currentWaterchangeState == WATER_CHANGE)
                   _currentWaterchangeState = WATER_IN;
                 else
                   _currentWaterchangeState = NONE;
                status_waterOut  = 0;
             }
           }
           else if(_currentWaterchangeState == WATER_IN)
           {
               if( E_OFF == IfToSwitchOnDepthSensorBasedDevice(depthInAquarium_FromTop_InMM , IdealdepthInAquarium_FromTop_InMM, deltaDepthInAquarium_InMM ))
                 _currentWaterchangeState = NONE;
           }
           else
                status_waterOut  = 0;


           if(_currentWaterchangeState == WATER_OUT || _currentWaterchangeState == WATER_CHANGE)
           {
              currentDeviceStatus[Aquarium_WaterOut_Device_Index] = true;
              currentDeviceStatus[Aquarium_WaterIn_Device_Index] = false;
           }
           else if(_currentWaterchangeState == WATER_IN)
           {
              currentDeviceStatus[Aquarium_WaterOut_Device_Index] = false;
              currentDeviceStatus[Aquarium_WaterIn_Device_Index] = true;
           }
           continue;
         }
      }

      if(!IsDeviceSensorBased(&(_deviceControls[i])))
      {
         bool isOn = false;
         if(i == Macro_Nutrient_Device_Index)
             isOn = _deviceControls[i].IsDeviceOnThisMin_BasedOnTimerAndOnStatus(&mlQuantityOfMacro);
         else if(i == Micro_Nutrient_Device_Index)
             isOn = _deviceControls[i].IsDeviceOnThisMin_BasedOnTimerAndOnStatus(&mlQuantityOfMicro);
         //else if(i == Food_Device_Index)
         //    isOn = _deviceControls[i].IsDeviceOnThisMin_BasedOnTimerAndOnStatus(&nbRoundsOfFood);
         else
         {
             isOn = _deviceControls[i].IsDeviceOnThisMin_BasedOnTimerAndOnStatus();
             if(!isOn)
                 isOn  = _deviceControls[i].IsDeviceOnThisMin_BasedOnUptoTime();
             if(!isOn && _deviceControls[i].HasAnyLightControlledTypeProgram())
             {
                for(int k = Light1_RedBlue16W_Device_Index; k<=Light6_White12W_Device_Index; k++)
                {
                     if(_deviceControls[k].IsDeviceOnThisMin_BasedOnTimerAndOnStatus(0))
                         isOn = true;
                     else if(_deviceControls[k].IsDeviceOnThisMin_BasedOnUptoTime())
                         isOn = true;
                }
             }
         }   
         currentDeviceStatus[i]  = isOn;
      }
      else 
      {
        status_Sensor_Device statusVal = E_NOCHANGE;
        if(i == Heater_Device_Index)
           statusVal = IfToSwitchOnTempSensorBasedDevice(tempOfAquarium_InMultipleOf10 ,CriticalTempOfAquarium_InMultipleOf10 , DeltaTempOfAquarium_InMultipleOf10  ,false);
        else if(i == Light_Fan_Device_Index)
           statusVal = IfToSwitchOnTempSensorBasedDevice(tempOfLight_InMultipleOf10 , CriticalTempOfLight_InMultipleOf10 , DeltaTempOfLight_InMultipleOf10  ,true);
        else if(i == Aquarium_WaterIn_Device_Index)
           statusVal = IfToSwitchOnDepthSensorBasedDevice(depthInAquarium_FromTop_InMM , IdealdepthInAquarium_FromTop_InMM, deltaDepthInAquarium_InMM);
        else if(i == Reservoir_WaterIn_Device_Index)
           statusVal = IfToSwitchOnDepthSensorBasedDevice(depthInReservoire_FromTop_InMM , IdealdepthInReservoire_FromTop_InMM, deltaDepthInReservoire_InMM );

        if(statusVal == E_ON)
            currentDeviceStatus[i]  = true;
        else if(statusVal == E_OFF)
            currentDeviceStatus[i]  = false;
      }
   }

   //makse sure macro, micro and food devices are not started twice in a minute
   if(currentDeviceStatus[Macro_Nutrient_Device_Index])
   {
      static int timeWhenMacroDeviceTurnedOn = 0;
      if( fabs(GetCurrentSecs() - timeWhenMacroDeviceTurnedOn) <= 60)//in same minute device should not switch on twice
         currentDeviceStatus[Macro_Nutrient_Device_Index] = false;
      else
         timeWhenMacroDeviceTurnedOn = GetCurrentSecs();
   }
   if(currentDeviceStatus[Micro_Nutrient_Device_Index])
   {
      static int timeWhenMicroDeviceTurnedOn = 0;
      if( fabs(GetCurrentSecs() - timeWhenMicroDeviceTurnedOn) <= 60)//in same minute device should not switch on twice
         currentDeviceStatus[Micro_Nutrient_Device_Index] = false;
      else
         timeWhenMicroDeviceTurnedOn = GetCurrentSecs();
   }
   /*
   if(currentDeviceStatus[Food_Device_Index])
   {
      static int timeWhenFoodDeviceTurnedOn = 0;
      if( fabs(GetCurrentSecs() - timeWhenFoodDeviceTurnedOn) <= 60)//in same minute device should not switch on twice
         currentDeviceStatus[Food_Device_Index] = false;
      else
         timeWhenFoodDeviceTurnedOn = GetCurrentSecs();
   }*/


   int triggerDist_Reservoir = reservoirDepth.GetMaxAllowedSensorBasedWaterOutInImm() - deltaDepthInReservoire_InMM;
   status_Sensor_Device statusVal_triggerDist_Reservoir = IfToSwitchOnDepthSensorBasedDevice(depthInReservoire_FromTop_InMM , triggerDist_Reservoir , deltaDepthInReservoire_InMM );
   if(statusVal_triggerDist_Reservoir == E_ON)
   {
      //water in reservoire is below lowest level
      _AquariumWaterNotFillingBecauseOfLowReservLevel = true;
      currentDeviceStatus[Aquarium_WaterIn_Device_Index] = false;
   }
   else if(statusVal_triggerDist_Reservoir == E_OFF)
   {
      _AquariumWaterNotFillingBecauseOfLowReservLevel = false;
   }

   int triggerDist_Aquarium = HeaterFilterOnMinimumWaterDistFromTopInMM - deltaDepthInAquarium_InMM;
   status_Sensor_Device statusVal_triggerDist_Aquarium = IfToSwitchOnDepthSensorBasedDevice(depthInAquarium_FromTop_InMM , triggerDist_Aquarium , deltaDepthInAquarium_InMM );
   if(statusVal_triggerDist_Aquarium == E_ON)
   {
      //water in aquarium is below HeaterFilterOnMinimumWaterDistFromTopInMM level
      _HeaterOffBecauseOfLowReservLevel = true;
      currentDeviceStatus[Filter_Device_Index] = false;
      currentDeviceStatus[Heater_Device_Index] = false;
   }
   else if(statusVal_triggerDist_Aquarium == E_OFF)
   {
      _HeaterOffBecauseOfLowReservLevel = false;
   }

   if(currentDeviceStatus[Light1_RedBlue16W_Device_Index] ||
      currentDeviceStatus[Light2_Red20W_Device_Index] ||
      currentDeviceStatus[Light3_White12W_Device_Index] ||
      currentDeviceStatus[Light4_White15W_Device_Index] ||
      currentDeviceStatus[Light5_White15W_Device_Index] ||
      currentDeviceStatus[Light6_White12W_Device_Index] ||
      currentDeviceStatus[Light_Fan_Device_Index] ||
      currentDeviceStatus[Macro_Nutrient_Device_Index] ||
      currentDeviceStatus[Micro_Nutrient_Device_Index] ||
      currentDeviceStatus[Aquarium_WaterOut_Device_Index] ||
      currentDeviceStatus[Aquarium_WaterIn_Device_Index] ||
      currentDeviceStatus[Reservoir_WaterIn_Device_Index] ||
      currentDeviceStatus[Chassis_Fan_Device_Index] /*||
      currentDeviceStatus[Food_Device_Index]*/)
      {
          if(digitalRead(Adapter_12V_Pin) == RELAY_OFF)
          {
             digitalWrite(Adapter_12V_Pin,RELAY_ON);
             delay(5000);//we wait because it takes time to switch on adapter. in case only food device is to be on, it will hamper some device control 
                      //because of non availability of adapter voltage.
          }
      }
      else
      {
          digitalWrite(Adapter_12V_Pin,RELAY_OFF);
      }


   //switch on and off the devices
   for(byte i = 0; i< AVAILABLE_RELAY_DEVICES; i++) 
   {
      if(_deviceControls[i].GetIfTimeInDurationOrNbOfRound())
      {
          if(!currentDeviceStatus[i])
            continue;

         int mlQualtityPerSecond = 2;

         if(i == Macro_Nutrient_Device_Index && mlQuantityOfMacro  > 0 )
         {
             InitFirstRow();
             AppendSpecialMessage(dispensingTxtName);
             InitSecondRow();
             AppendSpecialMessage(MacroNutTextName);
             
             DeviceOnOff(i,true);
             delay(((double)mlQuantityOfMacro/(double)mlQualtityPerSecond)*1000);
             DeviceOnOff(i,false);
         }
         else if(i == Micro_Nutrient_Device_Index && mlQuantityOfMicro > 0)
         {
             InitFirstRow();
             AppendSpecialMessage(dispensingTxtName);
             InitSecondRow();
             AppendSpecialMessage(MicroNutTextName);

             DeviceOnOff(i,true);
             delay(((double)mlQuantityOfMicro/(double)mlQualtityPerSecond)*1000);
             DeviceOnOff(i,false);
         }
         /*else if(i == Food_Device_Index && nbRoundsOfFood > 0)
         {
             InitFirstRow();
             AppendSpecialMessage(dispensingTxtName);
             InitSecondRow();
             AppendSpecialMessage(FoodTextName);

             StartSerialCommand();
             SendSerialCmdContent_Text(FOOD_COMMAND_NAME);
             SetSerialCommandRelimiter();
             SendSerialCmdContent_Byte(nbRoundsOfFood);
             FinishSerialCommand();

             delay(2000);//minimum delay
         }*/
      }
      else if(i == Reservoir_WaterIn_Device_Index)
      {
        int secondsToKeepReservoirKeepFilling = 1800;//every half hour toggle as the device gets heated.
        int secondsOFReservoirKeepFillingCycle = secondsToKeepReservoirKeepFilling+900;
        _reservoirWaterSolenoidCooling = false;
        if((GetCurrentSecs() % secondsOFReservoirKeepFillingCycle) > secondsToKeepReservoirKeepFilling)
        {
          //forcefully close reservoire filling because solenoid valve should be cooled
          _reservoirWaterSolenoidCooling = true; 
          DeviceOnOff(i, false);
        }
        else
        {
          DeviceOnOff(i, currentDeviceStatus[i]);
        }
      }
      else
      {
         DeviceOnOff(i, currentDeviceStatus[i]);
      }

   }

}
