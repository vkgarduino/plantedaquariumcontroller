IndividualDevice _deviceControls[AVAILABLE_RELAY_DEVICES] = {RBLightName,RLightName,W1LightName,W2LightName,W3LightName,W4LightName,LightFanName,Co2LightName,
                MacroNutTextName,MicroNutTextName,HeaterTextName,FilterTextName,AquariumWaterOutTextName,AquariumWaterInTextName,ReservoirWaterInTextName,ChassisFanName/*,FoodTextName*/};

void InitializeRelayControlledDevices()
{
  _deviceControls[Light1_RedBlue16W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light1_RedBlue16W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light1_RedBlue16W_Device_OnSlave_Index );
  _deviceControls[Light1_RedBlue16W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  byte hr = 10, min = 0;
  _deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  
   InitializeDeviceProgramOptions(_deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light1_RedBlue16W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);


  
  _deviceControls[Light2_Red20W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light2_Red20W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light2_Red20W_Device_OnSlave_Index );
  _deviceControls[Light2_Red20W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light2_Red20W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light2_Red20W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 0;
  _deviceControls[Light2_Red20W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light2_Red20W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  
  InitializeDeviceProgramOptions(_deviceControls[Light2_Red20W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light2_Red20W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);

  

  _deviceControls[Light3_White12W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light3_White12W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light3_White12W_Device_OnSlave_Index );
  _deviceControls[Light3_White12W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light3_White12W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light3_White12W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 9, min = 45;
  _deviceControls[Light3_White12W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light3_White12W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  
  InitializeDeviceProgramOptions(_deviceControls[Light3_White12W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light3_White12W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);




  _deviceControls[Light4_White15W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light4_White15W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light4_White15W_Device_OnSlave_Index );
  _deviceControls[Light4_White15W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light4_White15W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light4_White15W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 0;
  _deviceControls[Light4_White15W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light4_White15W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  
  InitializeDeviceProgramOptions(_deviceControls[Light4_White15W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light4_White15W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);



  _deviceControls[Light5_White15W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light5_White15W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light5_White15W_Device_OnSlave_Index );
  _deviceControls[Light5_White15W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light5_White15W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light5_White15W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 0;
  _deviceControls[Light5_White15W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light5_White15W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  
  InitializeDeviceProgramOptions(_deviceControls[Light5_White15W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light5_White15W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);



  _deviceControls[Light6_White12W_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[Light6_White12W_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light6_White12W_Device_OnSlave_Index );
  _deviceControls[Light6_White12W_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light6_White12W_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Light6_White12W_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 0;
  _deviceControls[Light6_White12W_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 13; min = 0;
  _deviceControls[Light6_White12W_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);

  InitializeDeviceProgramOptions(_deviceControls[Light6_White12W_Device_Index].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Light6_White12W_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);



  _deviceControls[Light_Fan_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Light_Fan_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Light_Fan_Device_OnSlave_Index );
  _deviceControls[Light_Fan_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Light_Fan_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,true/*sensor control*/,true/*any light on*/,false /*upto time*/);
  _deviceControls[Light_Fan_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_SensorControlled);



  _deviceControls[CO2_Device_Index].SetTotalNbPrograms(2);
  _deviceControls[CO2_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(CO2_Device_OnSlave_Index );
  _deviceControls[CO2_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[CO2_Device_Index].GetProgram(0),false/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[CO2_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 9, min = 45;
  _deviceControls[CO2_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 11; min = 0;
  _deviceControls[CO2_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);

  InitializeDeviceProgramOptions(_deviceControls[CO2_Device_Index].GetProgram(1),false/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[CO2_Device_Index].GetProgram(1)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 11, min = 30;
  _deviceControls[CO2_Device_Index].GetProgram(1)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 12; min = 20;
  _deviceControls[CO2_Device_Index].GetProgram(1)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);


  _deviceControls[Macro_Nutrient_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Macro_Nutrient_Device_Index].SetIfTimeInDuration(true);
  _deviceControls[Macro_Nutrient_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Macro_Nutrient_Pin);
  InitializeDeviceProgramOptions(_deviceControls[Macro_Nutrient_Device_Index].GetProgram(0),false/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Macro_Nutrient_Device_Index].GetProgram(0)->SetProgramDaySettingType(MWF);
  _deviceControls[Macro_Nutrient_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 30;
  _deviceControls[Macro_Nutrient_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 4/*duration 4 seconds*/; min = 0/*not relevant*/;
  _deviceControls[Macro_Nutrient_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);


  
  _deviceControls[Micro_Nutrient_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Micro_Nutrient_Device_Index].SetIfTimeInDuration(true);
  _deviceControls[Micro_Nutrient_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Micro_Nutrient_Pin);
  InitializeDeviceProgramOptions(_deviceControls[Micro_Nutrient_Device_Index].GetProgram(0),false/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Micro_Nutrient_Device_Index].GetProgram(0)->SetProgramDaySettingType(TuThSa);
  _deviceControls[Micro_Nutrient_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  hr = 10, min = 30;
  _deviceControls[Micro_Nutrient_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 4/*duration 4 seconds*/; min = 0/*not relevant*/;
  _deviceControls[Micro_Nutrient_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);



  
  _deviceControls[Heater_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Heater_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Heater_Pin);
   InitializeDeviceProgramOptions(_deviceControls[Heater_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,true/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Heater_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_SensorControlled);
 



  _deviceControls[Filter_Device_Index  ].SetTotalNbPrograms(1);
  _deviceControls[Filter_Device_Index  ].SetAssociatedDigitalPinOrSlaveDeviceIndex(Filter_Pin );
   InitializeDeviceProgramOptions(_deviceControls[Filter_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Filter_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_ON);




  _deviceControls[Aquarium_WaterOut_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Aquarium_WaterOut_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Aquarium_WaterOut_Pin  );
   InitializeDeviceProgramOptions(_deviceControls[Aquarium_WaterOut_Device_Index].GetProgram(0),true/*always on*/,false/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Aquarium_WaterOut_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_OFF);



  _deviceControls[Aquarium_WaterIn_Device_Index ].SetTotalNbPrograms(1);
  _deviceControls[Aquarium_WaterIn_Device_Index ].SetAssociatedDigitalPinOrSlaveDeviceIndex(Aquarium_WaterIn_Pin   );
   InitializeDeviceProgramOptions(_deviceControls[Aquarium_WaterIn_Device_Index].GetProgram(0),true/*always on*/,false/*timer control*/,true/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Aquarium_WaterIn_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_SensorControlled);



  _deviceControls[Reservoir_WaterIn_Device_Index ].SetTotalNbPrograms(1);
  _deviceControls[Reservoir_WaterIn_Device_Index ].SetAssociatedDigitalPinOrSlaveDeviceIndex(Reservoir_WaterIn_Pin   );
   InitializeDeviceProgramOptions(_deviceControls[Reservoir_WaterIn_Device_Index].GetProgram(0),true/*always on*/,false/*timer control*/,true/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Reservoir_WaterIn_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_SensorControlled);
  

  _deviceControls[Chassis_Fan_Device_Index].SetTotalNbPrograms(1);
  _deviceControls[Chassis_Fan_Device_Index].SetAssociatedDigitalPinOrSlaveDeviceIndex(Chassis_Fan_Device_OnSlave_Index );
  _deviceControls[Chassis_Fan_Device_Index].SetIfOnMaster(false);
  InitializeDeviceProgramOptions(_deviceControls[Chassis_Fan_Device_Index].GetProgram(0),true/*always on*/,true/*timer control*/,true/*sensor control*/,false/*any light on*/,false /*upto time*/);
  _deviceControls[Chassis_Fan_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_OFF);


  //_deviceControls[Food_Device_Index].SetTotalNbPrograms(1);
  //_deviceControls[Food_Device_Index].SetIfOnMaster(false);
  //_deviceControls[Food_Device_Index].SetIfTimeInDuration(true);
  //InitializeDeviceProgramOptions(_deviceControls[Food_Device_Index].GetProgram(0),false/*always on*/,true/*timer control*/,false/*sensor control*/,false/*any light on*/,false /*upto time*/);
  //_deviceControls[Food_Device_Index].GetProgram(0)->SetProgramOnOffControlType(Circuit_OFF);
  //hr = 10, min = 45;
  //_deviceControls[Food_Device_Index].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  //hr = 1/*nb round*/; min = 0/*not relevant*/;
  //_deviceControls[Food_Device_Index].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
}

void SlaveDeviceOnOff(byte iSlaveDeviceIndex, bool iOn)
{
   StartSerialCommand();
   SendSerialCmdContent_Text(LCD_DEVICE_CONTROL_COMMAND_NAME);
   SetSerialCommandRelimiter();
   SendSerialCmdContent_Byte(iSlaveDeviceIndex);
   SetSerialCommandRelimiter();
   if(iOn)
       SendSerialCmdContent_Text(CommandOn);
   else
      SendSerialCmdContent_Text(CommandOff);
   FinishSerialCommand();
}


void DeviceOnOff(byte iIndexOfDevice, bool iOn)
{
    byte pin = _deviceControls[iIndexOfDevice].GetAssociatedDigitalPinOrSlaveDeviceIndex();
    if(!_deviceControls[iIndexOfDevice].GetIfOnMaster())
    {
        SlaveDeviceOnOff(pin,iOn);
    }
    else
    {
       if(iIndexOfDevice == Filter_Device_Index)
         iOn = !iOn;//filetr is by default on.

       pinMode(pin,OUTPUT);
       if(iOn)
          digitalWrite(pin,RELAY_ON);
       else
          digitalWrite(pin,RELAY_OFF);
    }

}
