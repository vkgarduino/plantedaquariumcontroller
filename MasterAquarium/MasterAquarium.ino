
#define AQUARIUM_CONTROL
#define DISABLE_AQUARIUM_DEPTH_SETTING_MENU
#define DISABLE_RESERVOIRE_DEPTH_SETTING_MENU
#define DISABLE_LIGHT_TEMP_SETTING_MENU
#define DISABLE_TANK_TEMP_SETTING_MENU
#define DISABLE_CHASSIS_TEMP_SETTING_MENU

bool _reservoirWaterSolenoidCooling = false;
#include "SlaveConstants.h"
#include "Constants.h"
#include "Utils.h"
#include "Serialization.h"
#include "LCDControl.h"

#include "RTC.h"
#include "Remote.h"
#include "Devices.h"
#include "Devices_Init.h"
#include "Sensors.h"
#include "Menu.h"
#include "Menu_Devices.h"
#include "Menu_Sensors.h"
#include "Menu_WaterChange.h"
#include "Menu_Special.h"
#include "Menu_Init.h"

#include "DeviceControl.h"



void SaveSettings()
{
  StartReadingOrSaving();

  //started
  WriteValidityDigit();

  //each device menu 
  for(byte i = 0; i< AVAILABLE_RELAY_DEVICES; i++) 
  {
     WriteValidityDigit(2);
    _deviceControls[i].SaveInfo();
  }

  //temp sensor menus
  //WriteValidityDigit(3);
  //roomTemp.SaveInfo();

  #ifndef DISABLE_LIGHT_TEMP_SETTING_MENU
  WriteValidityDigit();
  lightTemp.SaveInfo();
  #endif 
  
  #ifndef DISABLE_TANK_TEMP_SETTING_MENU
  WriteValidityDigit(2);
  tankTemp.SaveInfo();
  #endif
  
  #ifndef DISABLE_CHASSIS_TEMP_SETTING_MENU
  WriteValidityDigit(3);
  chassisTemp.SaveInfo();
  #endif

  //depth sensor menus
  #ifndef DISABLE_AQUARIUM_DEPTH_SETTING_MENU
  WriteValidityDigit();
  aquariumDepth.SaveInfo();
  #endif

  #ifndef DISABLE_RESERVOIRE_DEPTH_SETTING_MENU
  WriteValidityDigit(2);
  reservoirDepth.SaveInfo();
  #endif

  //water chang menus
  /*
  Serial.println("");
  Serial.print("saving water change ");
  Serial.println((byte)_currentWaterchangeState);
  Serial.println(_currentWaterchangePercent);
  delay(1000);
*/
  WriteValidityDigit(3);
  UpdateValueFromCurrentAddressAndIncrementCurrentAddress((byte)_currentWaterchangeState);
  UpdateValueFromCurrentAddressAndIncrementCurrentAddress(_currentWaterchangePercent);



  //finished
  WriteValidityDigit();
  
}

bool ReadSettings(bool dummyTrialRun)
{
  _dummyTrialRun = dummyTrialRun;
  StartReadingOrSaving();

  
  //started
  if(!CheckValidityDigit())
    return false;

  //each device menu 
  for(byte i = 0; i< AVAILABLE_RELAY_DEVICES; i++) 
  {
    if(!CheckValidityDigit(2))
      return false;
    _deviceControls[i].ReadInfo();
  }

  //temp sensor menus
  //if(!CheckValidityDigit(3))
  //  return false;
  //roomTemp.ReadInfo();
  
  #ifndef DISABLE_LIGHT_TEMP_SETTING_MENU
  if(!CheckValidityDigit())
    return false;
  lightTemp.ReadInfo();
  #endif 
  
  #ifndef DISABLE_TANK_TEMP_SETTING_MENU
  if(!CheckValidityDigit(2))
    return false;
  tankTemp.ReadInfo();
  #endif
  
  #ifndef DISABLE_CHASSIS_TEMP_SETTING_MENU
  if(!CheckValidityDigit(3))
    return false;
  chassisTemp.ReadInfo();
  #endif
  
  //depth sensor menus
  #ifndef DISABLE_AQUARIUM_DEPTH_SETTING_MENU
  if(!CheckValidityDigit())
    return false;
  aquariumDepth.ReadInfo();
  #endif
  
  #ifndef DISABLE_RESERVOIRE_DEPTH_SETTING_MENU
  if(!CheckValidityDigit(2))
    return false;
  reservoirDepth.ReadInfo();
  #endif

  //water chang menus
  if(!CheckValidityDigit(3))
    return false;
  byte val = (byte)_currentWaterchangeState;
  ReadSerializedValue(val);
  _currentWaterchangeState = (current_State) val;
  ReadSerializedValue(_currentWaterchangePercent);

/*
  Serial.println("");
  Serial.print("Reading water change ");
  Serial.println((byte)_currentWaterchangeState);
  Serial.println(_currentWaterchangePercent);
  delay(1000);
*/
  
  //finished
  if(!CheckValidityDigit())
    return false;
    
  return true;
}

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  _AquariumWaterNotFillingBecauseOfLowReservLevel = false;
  _HeaterOffBecauseOfLowReservLevel = false;
  
   pinMode(Sensor_Aquarium_WaterDepth_Trigger_Pin, OUTPUT);
   pinMode(Sensor_Aquarium_WaterDepth_Echo_Pin, INPUT);
   digitalWrite(Sensor_Aquarium_WaterDepth_Trigger_Pin, LOW);

   pinMode(Sensor_Reservoir_WaterDepth_Trigger_Pin, OUTPUT);
   pinMode(Sensor_Reservoir_WaterDepth_Echo_Pin, INPUT);
   digitalWrite(Sensor_Reservoir_WaterDepth_Trigger_Pin, LOW);

  InitializeRelayControlledDevices();
  InitializeRtc();
  InitializeRemote();
  InitializeSettingMenus();
  DisplaySettingMenus();
  InitCurrentDeviceStatus();
  pinMode(Adapter_12V_Pin,OUTPUT);
  if(ReadSettings(true))
  {
     ReadSettings(false);//read the settings only if dummy run succeeds.
  }
}



int _lastDeviceCheckTime=0;
byte _deviceRefreshTimeInSecond = 2;

void loop() {
  delay(100);

  //check if rtc showing correct timings
  /*if(GetRtcTime().year() < 2019 || GetRtcTime().year() > 2030)
  {
     ShowVerySpecialMessage(wrongDateTimeError,1);
  }*/

  if(fabs(GetCurrentSecs() - _lastDeviceCheckTime) > _deviceRefreshTimeInSecond)
  {
    // control devices after every _deviceRefreshTimeInSecond seconds
    _lastDeviceCheckTime = GetCurrentSecs();
     ControlDevices();
  }
  

  byte btnPressed = GetRemotePressedButton();

  if(IsSensorScreenDisplayed()) //sensor screen shown, left/right pressed
  {
    if( btnPressed == REMOTE_RIGHT_BUTTON_PRESSED)
    {
      ForceInitializeScrrenType(GetNextValidScreenType(false));
      ForceScreenSaver();
    }
    else if(btnPressed == REMOTE_STAR_OR_BACK_BUTTON_PRESSED && GetCurrentSensorScreenVisible() != E_BlankScreen )
    {
      _isCurrentScreenLocked = !_isCurrentScreenLocked;
      if(!_isCurrentScreenLocked)
         ForceInitializeScrrenType(GetNextValidScreenType(false));
      ForceScreenSaver();
    }
    else if(btnPressed == REMOTE_LEFT_BUTTON_PRESSED)
    {
      ForceInitializeScrrenType(GetPrevValidScreenType(false));
      ForceScreenSaver();
    }
  }  
  
  
  if(GetSecondFromLastButtonPrssedInSecond() > FLASH_SCREEN_NO_BUTTON_PRESS_TIME)
  {

    if(GetSecondFromLastButtonPrssedInSecond() > LONG_TIME_NO_ACTIVITY_TIME)
    {
       ForceInitializeScrrenType(E_BlankScreen);
       SetIsSensorScreenDisplayed(false);
    }
    else if(!IsSensorScreenDisplayed())
    {
       SetIsSensorScreenDisplayed(true);
    }
    
    DisplaySensorParametersOfAquarium();
    delay(500);//update display parameter atleast after 1 second
  }
  else if(btnPressed == REMOTE_NO_BUTTON_PRESSED)
  {
  }
  else
  {
    LcdGetUp();
    if(IsSensorScreenDisplayed())
    {
       SetIsSensorScreenDisplayed(false);
       DisplaySettingMenus();
    }
    else
    {
      bool understoodBtnPressed = true;
      if( btnPressed == REMOTE_LEFT_BUTTON_PRESSED)
         RemoteLeftButtonPressed();
      else if( btnPressed == REMOTE_RIGHT_BUTTON_PRESSED)
         RemoteRightButtonPressed();
      else if( btnPressed == REMOTE_UP_BUTTON_PRESSED)
         RemoteUpButtonPressed();
      else if( btnPressed == REMOTE_DOWN_BUTTON_PRESSED) 
         RemoteDownButtonPressed();
      else if( btnPressed == REMOTE_STAR_OR_BACK_BUTTON_PRESSED)
         RemoteStarOrBackButtonPressed();
      else if( btnPressed == REMOTE_OK_OR_ENTER_BUTTON_PRESSED)
        RemoteOkOrEnterButtonPressed();
      else if( btnPressed >= REMOTE_0_BUTTON_PRESSED && btnPressed <= REMOTE_9_BUTTON_PRESSED)
        RemoteDigitPressed(btnPressed);
      else
      {
        understoodBtnPressed = false;
      }
      
      if(understoodBtnPressed)
        DisplaySettingMenus();
    }
  }

  SaveSettings();
}
