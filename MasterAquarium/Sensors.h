#include <OneWire.h>
#include <DallasTemperature.h>

class Sensor
{
  byte nameIndex;
  public:
   Sensor(const byte &iNameIndex):nameIndex(iNameIndex)  {  }
   byte GetNameIndex() { return nameIndex; }
 };


class TemperatureSensor : public Sensor
{
  byte data;//0,1,2,3,4 for pin
  int _CriticalTempInMultipleOf10;
  int _DeltaTempToSwitchOffSensedDevicesInMultipleOf10;
  public:
  TemperatureSensor(const byte &iName, byte signalPin,int iCriticalTemp,int iDeltaTempToSwitchOffSensedDevices): Sensor(iName)
  {
     _CriticalTempInMultipleOf10 = iCriticalTemp;
     _DeltaTempToSwitchOffSensedDevicesInMultipleOf10 = iDeltaTempToSwitchOffSensedDevices;
     data = 0;
     SetBitsInTarget(&data,signalPin,0,4);
  }

  void SetCriticalTemp_InMultipleOf10(int  iValue)
  {
      if(iValue > 999)
       iValue = 999;
     _CriticalTempInMultipleOf10 = iValue ;
  }
  
  int GetCriticalTemp_InMultipleOf10()  {   return _CriticalTempInMultipleOf10 ; }
  int GetDeltaTemp_InMultipleOf10()     {   return _DeltaTempToSwitchOffSensedDevicesInMultipleOf10 ; }
  byte GetPin()                         {return GetValueFromBits(&data,0,4);}
  virtual int GetTemp_InMultipleOf10() = 0;

  
  void SaveInfo()
  {
    byte byte1,byte2;
    byte1 = _CriticalTempInMultipleOf10/10;
    byte2 = _CriticalTempInMultipleOf10%10;
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte1);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte2);

    /*
    Serial.println("");
    Serial.print("Saving temp ");
    Serial.println(GetNameIndex());
    Serial.println(byte1);
    Serial.println(byte2);
    delay(1000); */   
  }
  void ReadInfo( )
  {
    byte byte1,byte2;
    byte1 = _CriticalTempInMultipleOf10/10;
    byte2 = _CriticalTempInMultipleOf10%10;

    ReadSerializedValue(byte1);
    ReadSerializedValue(byte2);

    _CriticalTempInMultipleOf10 = ((int)byte1)*10 + byte2;

    /*
    Serial.println("");
    Serial.print("Reading temp ");
    Serial.println(GetNameIndex());
    Serial.println(byte1);
    Serial.println(byte2);
    delay(1000);*/    
    
  }
  
};


#define TEMP_SENSOR_HAS_REVERSE_PIN

class AnalogTemperatureSensor : public TemperatureSensor
{
  public:
  AnalogTemperatureSensor(const byte &iName, byte signalPin,int iCriticalTemp,int iDeltaTempToSwitchOffSensedDevices): TemperatureSensor(iName,signalPin,iCriticalTemp,iDeltaTempToSwitchOffSensedDevices){}
  
  int GetTemp_InMultipleOf10()
  {
     int val = analogRead(GetPin());
     double Temp = log((10240000/(1023-val)-10000));
     Temp = 100 / (0.1129148 + (0.0234125 + (0.00000876741 * Temp * Temp ))* Temp );
     Temp = Temp-273.15;
     return Temp*10;
  }
};

OneWire oneWire(Sensor_TankTempeature_Pin);
DallasTemperature digitalTempSensor(&oneWire);

class DigitalTemperatureSensor : public TemperatureSensor
{
  public:
  DigitalTemperatureSensor(const byte &iName, byte signalPin,int iCriticalTemp,int iDeltaTempToSwitchOffSensedDevices): TemperatureSensor(iName,signalPin,iCriticalTemp,iDeltaTempToSwitchOffSensedDevices){}
  
  int GetTemp_InMultipleOf10()
  {
     digitalTempSensor.requestTemperatures();
     return digitalTempSensor.getTempCByIndex(0)*10;
  }
};


int GetDistanceInMM(byte iTriggerPin, byte iEchoPin)
{
   digitalWrite(iTriggerPin, LOW);
   delayMicroseconds(2);
   digitalWrite(iTriggerPin, HIGH);
   delayMicroseconds(10);
   digitalWrite(iTriggerPin, LOW);
   unsigned long duration = pulseIn(iEchoPin, HIGH);
   return (duration +10)* 0.17;
}

class UltrasonicSensor : public Sensor
{
  byte data;//0,1,2,3 for trigger pin, 4,5,6,7 for echo pin
  int _dist1ActualFromTopInMM,_dist2ActualFromTopInMM;
  int _dist1ShownValueFromTopInMM,_dist2ShownValueFromTopInMM;
  int _IdealDistFromTopInMM, _totalDepthOfTankInMM;
  int _tankLengthInMM, _tankWidthInMM;
  int _maxAllowedSensorBasedWaterOutInImm;
  public:
  
  UltrasonicSensor(const byte &iName, byte triggPin, byte echoPin, 
                        int dist1ActualFromTopInMM,int dist1ShownValueFromTopInMM,int dist2ActualFromTopInMM,int dist2ShownValueFromTopInMM,
                        int IdealDistFromTopInMM, int totalDepthOfTankInMM, int tankLengthInMM, int tankWidthInMM,
                        int maxAllowedSensorBasedWaterOutInImm): Sensor(iName)
  {
    data = 0;
    SetBitsInTarget(&data,triggPin,0,3);;
    SetBitsInTarget(&data,echoPin,4,7);;
    _dist1ActualFromTopInMM = dist1ActualFromTopInMM;
    _dist2ActualFromTopInMM = dist2ActualFromTopInMM;
    _dist1ShownValueFromTopInMM = dist1ShownValueFromTopInMM;
    _dist2ShownValueFromTopInMM = dist2ShownValueFromTopInMM;
    _IdealDistFromTopInMM = IdealDistFromTopInMM;
    _totalDepthOfTankInMM = totalDepthOfTankInMM;
    _tankLengthInMM = tankLengthInMM;
    _tankWidthInMM = tankWidthInMM;
    _maxAllowedSensorBasedWaterOutInImm = maxAllowedSensorBasedWaterOutInImm;
  }
  
  void SaveInfo()
  {
    byte byte1,byte2;
    byte1 = _IdealDistFromTopInMM/10;
    byte2 = _IdealDistFromTopInMM%10;
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte1);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte2);

    /*
    Serial.println("");
    Serial.print("Saving depth ");
    Serial.println(GetNameIndex());
    Serial.println(byte1);
    Serial.println(byte2);
    delay(1000);*/
  }
  void ReadInfo( )
  {
    byte byte1,byte2;
    byte1 = _IdealDistFromTopInMM/10;
    byte2 = _IdealDistFromTopInMM%10;

    ReadSerializedValue(byte1);
    ReadSerializedValue(byte2);

    _IdealDistFromTopInMM = ((int)byte1)*10 + byte2;

    /*
    Serial.println("");
    Serial.print("Reading depth ");
    Serial.println(GetNameIndex());
    Serial.println(byte1);
    Serial.println(byte2);
    delay(1000);*/
    
  }
  void SetIdealDistanceOfWaterLevelFromTopInMM(int idealDistanceInMM)
  {
     _IdealDistFromTopInMM = idealDistanceInMM;
  }

  
  int GetIdealDistanceOfWaterLevelFromTopInMM()
  {
    return _IdealDistFromTopInMM;
  }

  int GetMaxAllowedSensorBasedWaterOutInImm()
  {
    return _maxAllowedSensorBasedWaterOutInImm;
  }
  
  int GetWaterDistanceFromTopInMM()
  {

    int val1 = GetDistanceInMM(GetValueFromBits(&data,0,3),GetValueFromBits(&data,4,7));
    double slope = double(_dist2ActualFromTopInMM - _dist1ActualFromTopInMM)/double(_dist2ShownValueFromTopInMM - _dist1ShownValueFromTopInMM);

    int val2 = slope*((double)(val1-_dist1ShownValueFromTopInMM)) + _dist1ActualFromTopInMM;
    return val2;
  }

  int GetWaterOutDistanceFromTopInMM()
  {
     return ((_totalDepthOfTankInMM - _IdealDistFromTopInMM)*_currentWaterchangePercent)/100 + _IdealDistFromTopInMM;
  }
};


AnalogTemperatureSensor roomTemp(roomTempSensorName,Sensor_RoomTemperature_Pin, 300 /*no use sofar*/ /*critical temp * 10*/,20 /*dummy nb as no controlled device*/);
AnalogTemperatureSensor lightTemp(lightTempSensorName,Sensor_LightTemperature_Pin, 400 /*critical temp * 10*/,100 /*device switches off when temp is 10 degree less than critial temp*/);
DigitalTemperatureSensor tankTemp(tankTempSensorName,Sensor_TankTempeature_Pin, 260 /*critical temp * 10*/,10 /*device switches off when temp is 1 degree more than critial temp*/);
AnalogTemperatureSensor chassisTemp(chassisTempSensorName,Sensor_ChassisTempeature_Pin, 400 /*critical temp * 10*/,100 /*device switches off when temp is 10 degree less than critial temp*/);

/*aquariumDepth
distance from top of aquarium  shown values on sensor
1  2.7
1.5 2.9
2.2 3.5
2.9 3.85
3.9 4.65
4.5 5.1
5 5.5
5.5 5.9
6 6.35
6.7 6.8
7.5 7.4
8 7.85
8.6 8.2
9.1 8.6
9.5 9
10.1  9.5
10.6  9.75
11.2  10.3
11.9  10.6
12.2  11
12.7  11.45
13.4  11.9
13.9  12.3
14.3  12.6
15  13.1
15.5  13.5
16  13.9
16.5  14.3
17  14.8
17.6  15.2
18  15.7
18.5  16.2
19  16.25
19.5  16.6
20  17.1
20.6  17.5


 */

int HeaterFilterOnMinimumWaterDistFromTopInMM = 75;
UltrasonicSensor aquariumDepth(waterLevelSensorName,/*name*/
                               Sensor_Aquarium_WaterDepth_Trigger_Pin,
                               Sensor_Aquarium_WaterDepth_Echo_Pin,
                               10,27, /* 1 cm distance from aquarium, sensor value shows 9 cm*/
                               200,171,/* 20 cm distance from aquarium, sensor value shows 23.4 cm*/
                               35, /* ideal distane from top in mm*/
                               440, /*total depth mm*/
                               793, /*lenght in mm (internal)*/
                               400, /* witdth in mm(internal)*/
                               160
                               );

/*reservoirDepth
" distance from top of reservoire"        vs         "shown values on sensor"
1.2 6
2.1 6.95
3 7.5
3.9 8.45
5.6 9.8
7.1 9.7
8.4 10.65
9.1 11.95
10.6  13.1
12  14.8
13  14.8
14.1  16.3
15.5  17.5
16.8  18.2
18.5  19.5
20  20.6
21.5  21.7
23  22.7
24.2  23.6
25.6  25.1
28  26.7
29.5  27.8
30.8  28.9
32  29.8
33.2  30.5
34.6  31.5
35.6  32.4
37  33.5
38.2  34.5
39.2  35
40.5  36.05
42  37.3
43  38
45  39.4

 */


UltrasonicSensor reservoirDepth(reservLevelSensorName,/*name*/
                               Sensor_Reservoir_WaterDepth_Trigger_Pin,
                               Sensor_Reservoir_WaterDepth_Echo_Pin,
                               12,60, /* 1 cm distance from aquarium, sensor value shows 9 cm*/
                               430,380,/* 20 cm distance from aquarium, sensor value shows 23.4 cm*/
                               15, /* ideal distane from top in mm*/
                               450, /*total depth mm*/
                               360, /*lenght in mm (internal)*/
                               378, /* witdth in mm(internal)*/
                               360
                               );

enum ScreenType{E_BlankScreen,E_DateTime,E_AquariumTankFilling, E_AquariumTankDraining, E_ReservoireTankFilling,E_AquariumHeating, E_RoomAndChassisTemp, E_LightAndWaterTemp,E_TankReservoirWater};
int sensorScreenStartingTime_InSec=0;
bool _isCurrentScreenLocked = false;
bool _isSensorScreenDisplayed = false;

void InitCurrentSensorScreenDisplayTime()  { sensorScreenStartingTime_InSec = GetCurrentSecs(); }
int GetSecondsSinceSensorMenuDisplayed()   { return (int) ((GetCurrentSecs()-sensorScreenStartingTime_InSec));}
bool IsSensorScreenDisplayed(){return _isSensorScreenDisplayed;}
void SetIsSensorScreenDisplayed(bool ival){ _isSensorScreenDisplayed = ival;}



bool IfToDisplayCurrentScreen(ScreenType iType, bool isBlankScreenAllowed)
{
       if( !isBlankScreenAllowed && iType == E_BlankScreen)
          return false;
       if( (!currentDeviceStatus[Heater_Device_Index] && !_HeaterOffBecauseOfLowReservLevel)  && iType == E_AquariumHeating)
          return false;
       if( (!currentDeviceStatus[Aquarium_WaterIn_Device_Index] && !_AquariumWaterNotFillingBecauseOfLowReservLevel) && iType == E_AquariumTankFilling)
          return false;
       if( !currentDeviceStatus[Aquarium_WaterOut_Device_Index] && iType == E_AquariumTankDraining)
          return false;
       if( !currentDeviceStatus[Reservoir_WaterIn_Device_Index] && iType == E_ReservoireTankFilling)
          return false;
       return true;
}

ScreenType _WhichSensorScreenIsDisplayed = E_BlankScreen;
ScreenType _PrevDisplayedSensorScreen = E_BlankScreen;
void ForceInitializeScrrenType(ScreenType iType)
{
  if(_WhichSensorScreenIsDisplayed != _PrevDisplayedSensorScreen)
     _PrevDisplayedSensorScreen = _WhichSensorScreenIsDisplayed;
  _WhichSensorScreenIsDisplayed = iType;
  InitCurrentSensorScreenDisplayTime();
}

ScreenType GetCurrentSensorScreenVisible()
{
  return _WhichSensorScreenIsDisplayed;
}


ScreenType GetPreviouslyDisplayedSensorScreen()
{
  return _PrevDisplayedSensorScreen;
}

ScreenType GetNextValidScreenType(bool isBlankScreenAllowed)
{
  ScreenType type = GetCurrentSensorScreenVisible();
  while(1)
  {
     if(type == E_TankReservoirWater)
         type = E_BlankScreen;
     else
        type = (ScreenType)((byte)type + 1);

     if(IfToDisplayCurrentScreen(type, isBlankScreenAllowed))
         return type;
  }
  return E_BlankScreen;  
}


ScreenType GetPrevValidScreenType(bool isBlankScreenAllowed)
{
  ScreenType type = GetCurrentSensorScreenVisible();
  while(1)
  {
     if(type == E_BlankScreen)
       type = E_TankReservoirWater;
     else
       type = (ScreenType)((byte)type - 1);

     if(IfToDisplayCurrentScreen(type, isBlankScreenAllowed))
         return type;
  }
  return E_BlankScreen;  
}

void DisplaySensorParametersOfAquarium()
{
  SetLcdBlink(false);//no blinking.


  byte line_1_TitleNb = 0,line_2_TitleNb = 0;
  int line_1_DisplayedValue = 0, line_2_DisplayedValue = 0;
  byte line_1_UnitNb = 0, line_2_UnitNb = 0;

  int secondsPassed = GetSecondsSinceSensorMenuDisplayed();

  byte stoppingTime = FLASH_SCREEN_DISPLAY_TIME;
  if(GetCurrentSensorScreenVisible() == E_BlankScreen)
      stoppingTime = FLASH_SCREEN_SLEEP_TIME;    

  if(secondsPassed > stoppingTime)
  {
     if(!_isCurrentScreenLocked)
         ForceInitializeScrrenType(GetNextValidScreenType(true));
     else
     {
         if(E_BlankScreen == GetCurrentSensorScreenVisible())
             ForceInitializeScrrenType(GetPreviouslyDisplayedSensorScreen());
         else
             ForceInitializeScrrenType(E_BlankScreen);
     }
  }
  
  secondsPassed = secondsPassed%4;//for 2 sec we will show current menu, for other 2 sec expected menu in case of water draining or water filling.
  bool showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining = false;
  if(secondsPassed < 2)
      showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining = true;


  if(E_BlankScreen == GetCurrentSensorScreenVisible())
     LcdSleep();
  else
      LcdGetUp();

  if(GetCurrentSensorScreenVisible() == E_DateTime)
  {

         InitFirstRow();
         DateTime& now = GetRtcTime();
         PrintTime(now.hour(), now.minute(), now.second(),true);
         AppendTextInLcd(", ");
         AppendTextInLcd(weekDay[now.dayOfWeek()]);

         InitSecondRow();
         PrintDate(now.date(), now.month(), now.year());
  }
  else if(GetCurrentSensorScreenVisible() == E_AquariumTankFilling)
  {
         line_1_TitleNb = waterFillingTxtName;

         if(_AquariumWaterNotFillingBecauseOfLowReservLevel)
         {
            line_2_TitleNb = lowReservoirLevelTxtName; 
         }
         else if(showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining)
         {
            line_2_TitleNb = currentTxtName; 
            line_2_DisplayedValue = aquariumDepth.GetWaterDistanceFromTopInMM();
         }
         else
         {
            line_2_TitleNb = expectedTxtName; 
            line_2_DisplayedValue = aquariumDepth.GetIdealDistanceOfWaterLevelFromTopInMM();
         }
  }
  else if(GetCurrentSensorScreenVisible() == E_AquariumTankDraining)
  {
         line_1_TitleNb = waterDrainingTxtName;
         if(showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining)
         {
            line_2_TitleNb = currentTxtName; 
            line_2_DisplayedValue = aquariumDepth.GetWaterDistanceFromTopInMM();
         }
         else
         {
            line_2_TitleNb = expectedTxtName; 
            line_2_DisplayedValue = aquariumDepth.GetWaterOutDistanceFromTopInMM();
         }
  }       
  else if(GetCurrentSensorScreenVisible() == E_ReservoireTankFilling)
  {
         line_1_TitleNb = reservoirFillingTxtName;  
         if(_reservoirWaterSolenoidCooling)
         {
            line_2_TitleNb = solenoidCoolingTextName;
         }
         else if(showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining)
         {
            line_2_TitleNb = currentTxtName; 
            line_2_DisplayedValue = reservoirDepth.GetWaterDistanceFromTopInMM();
         }
         else
         {
            line_2_TitleNb = expectedTxtName; 
            line_2_DisplayedValue = reservoirDepth.GetIdealDistanceOfWaterLevelFromTopInMM();
         }
  }
  else if(GetCurrentSensorScreenVisible() == E_AquariumHeating)
  {
         line_1_TitleNb = aquariumHeatingTxtName;  
         if(_HeaterOffBecauseOfLowReservLevel)
         {
            line_2_TitleNb = waterBelowLevelTextName; 
         }
         else if(showCurrentInsteadOfExpectedTimingWhenWaterFillingOrDraining)
         {
            line_2_TitleNb = currentTxtName; 
            line_2_DisplayedValue = tankTemp.GetTemp_InMultipleOf10();
         }
         else
         {
            line_2_TitleNb = expectedTxtName; 
            line_2_DisplayedValue = tankTemp.GetCriticalTemp_InMultipleOf10() + tankTemp.GetDeltaTemp_InMultipleOf10();
         }
  }
  else if(GetCurrentSensorScreenVisible() == E_RoomAndChassisTemp)
  {
       line_1_TitleNb = roomTempSensorName;  
       line_1_DisplayedValue = roomTemp.GetTemp_InMultipleOf10();
       line_1_UnitNb = degCTextName; 
       line_2_TitleNb = chassisTempSensorName;  
       line_2_DisplayedValue = chassisTemp.GetTemp_InMultipleOf10();
       line_2_UnitNb = degCTextName; 
  }
  else if(GetCurrentSensorScreenVisible() == E_LightAndWaterTemp)
  {
       line_1_TitleNb = lightTempSensorName;  
       line_1_DisplayedValue = lightTemp.GetTemp_InMultipleOf10();
       line_1_UnitNb = degCTextName; 
       line_2_TitleNb = tankTempSensorName;  
       line_2_DisplayedValue = tankTemp.GetTemp_InMultipleOf10();
       line_2_UnitNb = degCTextName; 
  }
  else if(GetCurrentSensorScreenVisible() == E_TankReservoirWater)
  {
       line_1_TitleNb = waterLevelSensorName;  
       line_1_DisplayedValue = aquariumDepth.GetWaterDistanceFromTopInMM();
       line_1_UnitNb = cmTxtName; 
       line_2_TitleNb = reservLevelSensorName;  
       line_2_DisplayedValue = reservoirDepth.GetWaterDistanceFromTopInMM();
       line_2_UnitNb = cmTxtName; 
  }

  if(GetCurrentSensorScreenVisible() == E_BlankScreen)
  {
     InitBothRow();//initialize both rows
  }
  else if(GetCurrentSensorScreenVisible() != E_DateTime)
  {
    InitFirstRow();
    if(line_1_TitleNb > 0)
       AppendSpecialMessage(line_1_TitleNb);
    if(line_1_DisplayedValue > 0)
       AppendDouble_DenoteAsMultipleOf10(line_1_DisplayedValue);

    InitSecondRow();
    if(line_2_TitleNb > 0)
       AppendSpecialMessage(line_2_TitleNb);
    if(line_2_DisplayedValue > 0)
       AppendDouble_DenoteAsMultipleOf10(line_2_DisplayedValue);
  }


  if(_isCurrentScreenLocked)
  {
        SetLcdCursor(15,1);
        AppendTextInLcd("*");
  }
}
