#include <IRremote.h>
IRrecv irReceiver(Remotel_Pin);
int _lastBtnPressedTime=0;

void InitializeRemote()
{
   irReceiver.enableIRIn();
   _lastBtnPressedTime = GetCurrentSecs();
}

void ForceScreenSaver()
{
    _lastBtnPressedTime = GetCurrentSecs()-FLASH_SCREEN_NO_BUTTON_PRESS_TIME-1/*1 is just dummy second value*/; // so that display screen remains displayed
}


const byte  REMOTE_0_BUTTON_PRESSED  =0;
const byte  REMOTE_1_BUTTON_PRESSED  =1;
const byte  REMOTE_2_BUTTON_PRESSED  =2;
const byte  REMOTE_3_BUTTON_PRESSED  =3;
const byte  REMOTE_4_BUTTON_PRESSED  =4;
const byte  REMOTE_5_BUTTON_PRESSED  =5;
const byte  REMOTE_6_BUTTON_PRESSED  =6;
const byte  REMOTE_7_BUTTON_PRESSED  =7;
const byte  REMOTE_8_BUTTON_PRESSED  =8;
const byte  REMOTE_9_BUTTON_PRESSED  =9;
const byte  REMOTE_LEFT_BUTTON_PRESSED  =10;
const byte REMOTE_RIGHT_BUTTON_PRESSED  =11;
const byte  REMOTE_UP_BUTTON_PRESSED  =12;
const byte  REMOTE_DOWN_BUTTON_PRESSED  =13;
const byte  REMOTE_STAR_OR_BACK_BUTTON_PRESSED  =14;
const byte  REMOTE_OK_OR_ENTER_BUTTON_PRESSED  =15;
const byte  REMOTE_HASH_OR_INFO_BUTTON_PRESSED  =16;
const byte  REMOTE_NO_BUTTON_PRESSED  =255;
/// *************************************************************** REMOTE RELATED ********************************************

int GetSecondFromLastButtonPrssedInSecond()
{
     int val = (int) ((GetCurrentSecs()-_lastBtnPressedTime));
     if(val < 0)
       val = -1*val;
     return val;
}

//#define Consider_IRRemote_1
#define Consider_Onkyo_Remote_DVDMODE
//#define Consider_TataSky_Remote

const byte TryToDecodeButton(unsigned long &iDecodedValue)
{
switch(iDecodedValue)
{
#ifdef Consider_IRRemote_1  
  case 0xFF10EF:
  case 0x8C22657B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B3639C6:
#endif
#ifdef Consider_TataSky_Remote
  case 0xC0005A:
  case 0xCF7095CD:
#endif
     return REMOTE_LEFT_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF5AA5:
  case 0x449E79F:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B36B946:
#endif
#ifdef Consider_TataSky_Remote
  case 0xC0005B:
  case 0xB23554F:
#endif
     return REMOTE_RIGHT_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF18E7:
  case 0x3D9AE3F7:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B3659A6:
#endif
#ifdef Consider_TataSky_Remote
  case 0xC00058:
  case 0xA215B7E2:
#endif
     return REMOTE_UP_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF4AB5:
  case 0x1BC0157B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B36D926:
#endif
#ifdef Consider_TataSky_Remote
  case 0xC00059:
  case 0xA86E19D3:
#endif
     return REMOTE_DOWN_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF38C7:
  case 0x488F3CBB:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B367986:
#endif
#ifdef Consider_TataSky_Remote
  case 0xC0005C:
  case 0x9E856ADE:
#endif
     return REMOTE_OK_OR_ENTER_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF6897:
  case 0xC101E57B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B36F906:
#endif
#ifdef Consider_TataSky_Remote
  case 0x798DECFE:
  case 0xC00083:
#endif
     return REMOTE_STAR_OR_BACK_BUTTON_PRESSED;
       
#ifdef Consider_IRRemote_1  
  case 0xFF9867:
  case 0x97483BFB:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
 case 0x4B35E817:
#endif
#ifdef Consider_TataSky_Remote
  case 0x59B558AC:
  case 0x3C3007D4:
#endif
     return REMOTE_0_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFFA25D:
  case 0xE318261B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B35708F:
#endif
#ifdef Consider_TataSky_Remote
  case 0x7D03709:
  case 0xC00001:
#endif
     return REMOTE_1_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF629D:
  case 0x511DBB:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B35F00F:
#endif
#ifdef Consider_TataSky_Remote
  case 0x7AD7A7D9:
  case 0xC00002:
#endif
       return REMOTE_2_BUTTON_PRESSED;
     
#ifdef Consider_IRRemote_1  
  case 0xFFE21D:
  case 0xEE886D7F:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B3508F7:
#endif
#ifdef Consider_TataSky_Remote
  case 0x77D7A320:
  case 0xC00003:
#endif
     return REMOTE_3_BUTTON_PRESSED;
     
#ifdef Consider_IRRemote_1  
  case 0xFF22DD:
  case 0x52A3D41F:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B358877:
#endif
#ifdef Consider_TataSky_Remote
  case 0x1FF256EE:
  case 0xC00004:
#endif
     return REMOTE_4_BUTTON_PRESSED;

#ifdef Consider_IRRemote_1  
  case 0xFF02FD:
  case 0xD7E84B1B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B3548B7:
#endif
#ifdef Consider_TataSky_Remote
  case 0xDFF8AB96:
  case 0xC00005:
#endif
     return REMOTE_5_BUTTON_PRESSED;


#ifdef Consider_IRRemote_1  
  case 0xFFC23D:
  case 0x20FE4DBB:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B35C837:
#endif
#ifdef Consider_TataSky_Remote
  case 0x5CD90715:
  case 0xC00006:
#endif
     return REMOTE_6_BUTTON_PRESSED;          
     

#ifdef Consider_IRRemote_1  
  case 0xFFE01F:
  case 0xF076C13B:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B3528D7:
#endif
#ifdef Consider_TataSky_Remote
  case 0x0C00007:
  case 0x5FD90BCC:
#endif
     return REMOTE_7_BUTTON_PRESSED;


#ifdef Consider_IRRemote_1  
  case 0xFFA857:
  case 0xA3C8EDDB:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B35A857:
#endif
#ifdef Consider_TataSky_Remote
  case 0x54BEF27E:
  case 0xC00008:
#endif
     return REMOTE_8_BUTTON_PRESSED;  
     
#ifdef Consider_IRRemote_1  
  case 0xFF906F:
  case 0xE5CFBD7F:
#endif
#ifdef Consider_Onkyo_Remote_DVDMODE  
  case 0x4B356897:
#endif
#ifdef Consider_TataSky_Remote
  case 0x5D69140F:
  case 0xC00009:
#endif
     return REMOTE_9_BUTTON_PRESSED;  
             
   default:
      return REMOTE_NO_BUTTON_PRESSED;
  }
  return REMOTE_NO_BUTTON_PRESSED;
}

//#define DEBUG_REMOTE

byte GetRemotePressedButton()
{
 decode_results irReceiverResults;
 if (irReceiver.decode(&irReceiverResults))
 {
    #ifdef DEBUG_REMOTE
    InitBothRow();
    LcdGetUp();
    lcd.print(irReceiverResults.value,HEX);
    #endif
    irReceiver.resume();
    byte code = TryToDecodeButton(irReceiverResults.value);
    if(code != REMOTE_NO_BUTTON_PRESSED)
    {
          #ifdef DEBUG_REMOTE
          InitSecondRow();
          if( code == REMOTE_LEFT_BUTTON_PRESSED)
            lcd.print("left");
         else if( code == REMOTE_RIGHT_BUTTON_PRESSED)
            lcd.print("right");
         else if( code == REMOTE_UP_BUTTON_PRESSED)
            lcd.print("up");
         else if( code == REMOTE_DOWN_BUTTON_PRESSED) 
            lcd.print("down");
         else if( code == REMOTE_STAR_OR_BACK_BUTTON_PRESSED)
            lcd.print("back");
         else if( code == REMOTE_OK_OR_ENTER_BUTTON_PRESSED)
            lcd.print("ok");
         else if( code >= REMOTE_0_BUTTON_PRESSED && code <= REMOTE_9_BUTTON_PRESSED)
            lcd.print("digit");
       #endif
            
       _lastBtnPressedTime = GetCurrentSecs();
    }
    #ifdef DEBUG_REMOTE
    delay(1000);
    #endif
    return code;
 }  
 return REMOTE_NO_BUTTON_PRESSED;
}
