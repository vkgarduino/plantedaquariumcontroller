void InitializeSettingMenus()
{
  MainMenu * mainMenu = new MainMenu(4,emptyName);

  const byte _nbSpecialMenus = 2;
  static SpecialMenuEachItemInfo specialMenuItems[_nbSpecialMenus];
   specialMenuItems[0].SetInfo(allLightsOnTextName, 6, false);
   specialMenuItems[0].AppendDeviceIndex(Light1_RedBlue16W_Device_Index);
   specialMenuItems[0].AppendDeviceIndex(Light2_Red20W_Device_Index);
   specialMenuItems[0].AppendDeviceIndex(Light3_White12W_Device_Index);
   specialMenuItems[0].AppendDeviceIndex(Light4_White15W_Device_Index);
   specialMenuItems[0].AppendDeviceIndex(Light5_White15W_Device_Index);
   specialMenuItems[0].AppendDeviceIndex(Light6_White12W_Device_Index);


   specialMenuItems[1].SetInfo(oneLightTextName, 1, false);
   specialMenuItems[1].AppendDeviceIndex(Light3_White12W_Device_Index);
  
  Menu_SpecialMenu * specialMenu = new Menu_SpecialMenu(mainMenu,uptoTimeTextName,_nbSpecialMenus,specialMenuItems);
  mainMenu->AddItem(specialMenu);
  mainMenu->AddItem(new RelayControlledDeviceMenu(mainMenu,devicesTextName));

  #ifndef DISABLE_AQUARIUM_DEPTH_SETTING_MENU
  mainMenu->AddItem(new UltrasonicSensorMenu(mainMenu,&aquariumDepth,aquariumDepth.GetNameIndex()));
  #endif

  #ifndef DISABLE_RESERVOIRE_DEPTH_SETTING_MENU
  mainMenu->AddItem(new UltrasonicSensorMenu(mainMenu,&reservoirDepth,reservoirDepth.GetNameIndex()));
  #endif
  
  #ifndef DISABLE_LIGHT_TEMP_SETTING_MENU
  mainMenu->AddItem(new TemperatureSensorMenu(mainMenu,&lightTemp,lightTemp.GetNameIndex()));
  #endif
  
  #ifndef DISABLE_TANK_TEMP_SETTING_MENU
  mainMenu->AddItem(new TemperatureSensorMenu(mainMenu,&tankTemp,tankTemp.GetNameIndex()));
  #endif
  mainMenu->AddItem(new DateAndTimeMenu(mainMenu,dateTimeTextName));
  mainMenu->AddItem(new WaterChangeMenu(mainMenu,waterChangeTextName));



  _currentlyActiveMenu = specialMenu;
}
