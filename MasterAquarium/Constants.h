//heater, extra light, filter
/*
 * //any irrelevant pin set to INVALID_PIN
 * lights - 3
 * fan - 1
 * Extra light - 1?
 * CO2 -2
 * Macro, Micro - 2
 * Iron - 1?
 * Water In - 1
 * Watter out - 1? 
 *   --PURPLE is digital pin
 *   --Blue is vcc
 *   --Green is gound
 * 
 * BeepOnOff - 1? --currently set at digital 0
 * water Temp - 1
 * Remote - 1
 * Heater - 1?
 * Remote - 1
 * 
 * Temperature - 2
 * water depth- 1
 * 
 * 
 * RTC I2C      - Analog A3,A4
 * Display I2C - Analog A3,A4
 */



//RELAY CONTROLLED DEVICES DEVICES
#define Remotel_Pin 2
#define Macro_Nutrient_Pin 3
#define Micro_Nutrient_Pin 4
#define Reservoir_WaterIn_Pin 5
#define Aquarium_WaterOut_Pin 6
#define Aquarium_WaterIn_Pin 7
#define Sensor_TankTempeature_Pin  8
#define Sensor_Aquarium_WaterDepth_Trigger_Pin 9
#define Sensor_Aquarium_WaterDepth_Echo_Pin 10
#define Sensor_Reservoir_WaterDepth_Trigger_Pin 11
#define Sensor_Reservoir_WaterDepth_Echo_Pin 12
//pin 13 misbehaves
#define Sensor_ChassisTempeature_Pin A0 
#define Filter_Pin A1 
#define Heater_Pin A2
#define Adapter_12V_Pin A3
//a4,a5 are for rtc
#define Sensor_RoomTemperature_Pin A6
#define Sensor_LightTemperature_Pin A7
//a6,a7 are analog input only pin can can not be used as digital output


#define Light1_RedBlue16W_Device_Index 0
#define Light2_Red20W_Device_Index 1
#define Light3_White12W_Device_Index 2
#define Light4_White15W_Device_Index 3
#define Light5_White15W_Device_Index 4
#define Light6_White12W_Device_Index 5
#define Light_Fan_Device_Index 6
#define CO2_Device_Index 7
#define Macro_Nutrient_Device_Index 8
#define Micro_Nutrient_Device_Index 9
#define Heater_Device_Index 10
#define Filter_Device_Index 11
#define Aquarium_WaterOut_Device_Index 12
#define Aquarium_WaterIn_Device_Index 13
#define Reservoir_WaterIn_Device_Index 14
#define Chassis_Fan_Device_Index 15
//#define Food_Device_Index 16
#define AVAILABLE_RELAY_DEVICES 16



char weekDay[7][4] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
enum DayEnum{SUNDAY=0,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY};

#define FLASH_SCREEN_NO_BUTTON_PRESS_TIME 10
#define FLASH_SCREEN_SLEEP_TIME 6
#define FLASH_SCREEN_DISPLAY_TIME 4
#define LONG_TIME_NO_ACTIVITY_TIME 60 //in seconds

enum current_State{NONE, WATER_OUT,WATER_CHANGE,WATER_IN};
current_State _currentWaterchangeState = NONE;
byte _currentWaterchangePercent;

enum status_Sensor_Device{E_ON,E_OFF,E_NOCHANGE};
bool currentDeviceStatus[AVAILABLE_RELAY_DEVICES];
bool _AquariumWaterNotFillingBecauseOfLowReservLevel = false;
bool _HeaterOffBecauseOfLowReservLevel = false;

byte maxTimeOfSpecialMenu = 1; //not reached 
