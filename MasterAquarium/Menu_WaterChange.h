
class WaterChangeMenu : public Menu
{
      //GetHorizontalLineIndex 0 for title
      //GetHorizontalLineIndex 1 for water out only
      //GetHorizontalLineIndex 2 for water change
      //GetHorizontalLineIndex 3 for water change stop
      byte percentage;
      bool _ask,_reask;
  public: 
      void SetDefaultBlinking()
      {
         percentage = _currentWaterchangePercent;

         if(GetHorizontalLineIndex() == 1)
            SetBlinkingIndex(11);
         else if(GetHorizontalLineIndex() == 2)
            SetBlinkingIndex(14);
        _ask = false;_reask = false;
      }

     WaterChangeMenu(Menu *iParent, byte iNameIndex):Menu(iNameIndex)
     {
        _currentWaterchangePercent = 15;
        percentage = _currentWaterchangePercent;
        _currentWaterchangeState = NONE;
        SetHorizontalLineIndex(0);
        SetBlinkingIndex(0);
        Init(iParent,0);
        SetDefaultBlinking();
     }
     void GoPrevious()
     {
       percentage = percentage - 5;
       if(percentage < 15)
          percentage = 60;
        _ask = false;_reask = false;
     }
     void GoNext()
     {
       percentage = percentage +5;
       if(percentage > 60)
          percentage = 15;
        _ask = false;_reask = false;
     }


      void GoUp()
      {
        if(GetHorizontalLineIndex() > 0)
             SetHorizontalLineIndex(GetHorizontalLineIndex()-1);

        SetDefaultBlinking();
      }
      
      void GoDown()
      {
        if(GetHorizontalLineIndex() < 3)
           SetHorizontalLineIndex(GetHorizontalLineIndex()+1);

        SetDefaultBlinking();
      }

      void OnOk()
      {
          if(GetHorizontalLineIndex() == 0)
          {
            GoDown();
          }
          else if(!_ask)
          {
            _ask = true;
          }
          else if(GetHorizontalLineIndex() == 3)
          {
              _currentWaterchangeState = NONE;
             DeviceOnOff(Aquarium_WaterOut_Device_Index,false);           
            _ask = false, _reask = false;
            SetHorizontalLineIndex(0);
          }
          else if(!_reask)
          {
            _reask = true;
          }
          else
          {
            _currentWaterchangePercent = percentage;
            if(GetHorizontalLineIndex() == 1)
              _currentWaterchangeState = WATER_OUT;
            else
              _currentWaterchangeState = WATER_CHANGE;
            _ask = false, _reask = false;
          }
      }

      void Display()
      {
         SetLcdBlink(false);
         if(_reask)
         {
           InitBothRow();
           AppendSpecialMessage(askIfPipIsKeptInDrain);
           return;
         }
         else if(_ask)
         {
           InitBothRow();
           AppendSpecialMessage(askIfWaterChange);
           return;
         }
         InitFirstRow();
         AppendSpecialMessage(GetMenuNameIndex());
         if(GetHorizontalLineIndex() > 0)
            AppendSpecialMessage(setTextName);

         InitSecondRow();
         if(GetHorizontalLineIndex() == 0)
         {
            AppendSpecialMessage(backOkTextName);
         }
         else if(GetHorizontalLineIndex() ==1)
         {
           AppendSpecialMessage(waterOut);
           AppendByte(percentage);
         }  
         else if(GetHorizontalLineIndex() ==2)
         {
           AppendSpecialMessage(waterChange);
           AppendByte(percentage);
         }  
         else if(GetHorizontalLineIndex() ==3)
         {
           AppendSpecialMessage(stopWaterChange);
         }           
         if( GetHorizontalLineIndex() > 0)
         {
           SetLcdCursor(GetBlinkingIndex() , 1);
           SetLcdBlink(true);
         }
      }


     void OnDigitPress(byte iDigit)
     {

     }
};
