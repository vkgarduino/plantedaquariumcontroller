const byte LCD_NB_COLUMNS = 16;
const byte LCD_NB_ROWS = 2;

#define NB_DEVICES_IN_SLAVE 10
const byte   COMMAND_TYPE1_PIN_INDENTIFIER[NB_DEVICES_IN_SLAVE] = {2,3,4,5,6,7,8,A1,A0,A3};

const byte STEPPER_PIN_A = 9; // IN1 on the ULN2003 driver 1
const byte STEPPER_PIN_B = 10; // IN2 on the ULN2003 driver 1
const byte STEPPER_PIN_C = 11; // IN3 on the ULN2003 driver 1
const byte STEPPER_PIN_D = 12; // IN4 on the ULN2003 driver 1

const byte totalMenuItems = waterBelowLevelTextName+1;
const char *_ArrayNames[totalMenuItems] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; 

class InitVariables
{
  public:
  InitVariables()
  {
_ArrayNames[emptyName] = "";
_ArrayNames[RBLightName] = "Lgt_R&B";
_ArrayNames[RLightName] = "Lgt_R";
_ArrayNames[W1LightName] = "Lgt_Wht1";
_ArrayNames[W2LightName] = "Lgt_Wht2";
_ArrayNames[W3LightName] = "Lgt_Wht3";
_ArrayNames[W4LightName] = "Lgt_Wht4";
_ArrayNames[LightFanName] = "Light Fan";
_ArrayNames[Co2LightName] = "CO2";//8
_ArrayNames[MacroNutTextName] = "Macro Nut.";
_ArrayNames[MicroNutTextName] = "Micro Nut.";
_ArrayNames[HeaterTextName] = "Heater";
_ArrayNames[FilterTextName] = "Filter";
_ArrayNames[AquariumWaterOutTextName] = "Water out";
_ArrayNames[AquariumWaterInTextName] = "Water in";
_ArrayNames[ReservoirWaterInTextName] = "Reserv Wat In";
_ArrayNames[ChassisFanName] = "Chassis Fan";
_ArrayNames[roomTempSensorName] = "Room Temp: ";
_ArrayNames[lightTempSensorName] = "Lght Temp: ";
_ArrayNames[tankTempSensorName] = "Tank Temp: ";
_ArrayNames[chassisTempSensorName] = "Case Temp: ";
_ArrayNames[waterLevelSensorName] = "Water lev: ";
_ArrayNames[reservLevelSensorName] = "Reser lev: ";
_ArrayNames[dayTextName] = "Day: ";
_ArrayNames[modeTextName] = "Mode: ";
_ArrayNames[setTextName] = " Set";
_ArrayNames[closestTextName] = "Closest(cm):";
_ArrayNames[maxTextName] = "Maximum(cm):";
_ArrayNames[critTempTextName] = "Crit Temp: ";
_ArrayNames[dateTextName] = "Date: ";
_ArrayNames[dateTimeTextName] = "Date Time";
_ArrayNames[devicesTextName] = "Devices";
_ArrayNames[timeTextNameSpaceAfterTime] = "Time: ";
_ArrayNames[wrongDigitTextName] = "  Wrong Digit.  ";
_ArrayNames[backOkTextName] = "*back; Ok to set";
_ArrayNames[settingTextName] = " Setting";
_ArrayNames[sensorBasedTextName] = "Sensor based";
_ArrayNames[anyLightTextName] = "Any Light";
_ArrayNames[timerBasedTextName] = "Timer based";
_ArrayNames[dailyTextName] = "Daily";
_ArrayNames[MWFTextName] = "MWF";
_ArrayNames[TuThSaTextName] = "TuThSa";
_ArrayNames[okToSettingTextName] = "  Ok to Setting";
_ArrayNames[mlTextName] = "ml";
_ArrayNames[turnsTextName] = "Rnd";
_ArrayNames[okToSaveTextName] = "Press ok to save.";
_ArrayNames[errorTextName] = "Error Occured";
_ArrayNames[waterOut] = "Water Out: ";
_ArrayNames[waterChange] = "Water Change: ";
_ArrayNames[stopWaterChange] = "stop water change";
_ArrayNames[askIfWaterChange] = "Sure?";
_ArrayNames[askIfPipIsKeptInDrain] = "Pipe in drain?";
_ArrayNames[waterChangeTextName] = "Water Change";
_ArrayNames[waterDrainingTxtName] = "Water Draining";
_ArrayNames[waterFillingTxtName] = "Water Filling";
_ArrayNames[degCTextName] = " deg C.";
_ArrayNames[wrongDateTimeError] = "Wrong data time";
_ArrayNames[OkLevelTextName] = "OK (";
_ArrayNames[cmTxtName] = "cm";
_ArrayNames[idealLevelTxtName] = "Dist to Top:";
_ArrayNames[currentTxtName] = "Current: ";
_ArrayNames[expectedTxtName] = "Expected: ";
_ArrayNames[reservoirFillingTxtName] = "Reserv Filling";
_ArrayNames[lowReservoirLevelTxtName] = "Low Reserv Level";
_ArrayNames[aquariumHeatingTxtName] = "Tank Heating";
_ArrayNames[timeTextNameNoSpaceAfterTime] = "Time:";
_ArrayNames[dispensingTxtName] = "Dispensing ";
_ArrayNames[FoodTextName] = "Food";
_ArrayNames[uptoTimeBasedTextName] = "upto time";
_ArrayNames[MTWTFTextName] = "Weekday";
_ArrayNames[SaSuTextName] = "Weekend";
_ArrayNames[allLightsOnTextName] = "All Lights";
_ArrayNames[durationForUptoTIme] = "Duration :";
_ArrayNames[uptoTimeTextName] = "Turn-On till";
_ArrayNames[oneLightTextName] = "Low Lights";
_ArrayNames[solenoidCoolingTextName] = "Solenoid Cooling";
_ArrayNames[waterBelowLevelTextName] = "LOW WATER";
  }
};
InitVariables variables;
  

const char * DecodeTextMessage(int iNb)
{
  if(iNb < 0 || iNb >= totalMenuItems)
    return 0;
  return _ArrayNames[iNb];
}
