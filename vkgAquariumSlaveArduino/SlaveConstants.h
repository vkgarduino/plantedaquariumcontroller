#define RELAY_ON LOW
#define RELAY_OFF HIGH

#define ShowErrorMessages
//#define ShowSlaveActionMsgs

const char* MASTER_COMMAND_PREFIX = "$%";
const char* MASTER_COMMAND_POSTFIX = "%$";
const char* MASTER_COMMAND_RELIMITER = "#";
const char* CommandOn = "On";
const char* CommandOff = "Off";


const char* LCD_SET_CURSOR_COMMAND_NAME = "C";// LCD_SET_CURSOR_COMMAND_NAME##COLUMNNB##ROWNB
const char* LCD_DEVICE_CONTROL_COMMAND_NAME = "D";// LCD_APPEND_TEXT_VIA_MSGNB_COMMAND_NAME##MSGNB
const char* FOOD_COMMAND_NAME = "F";// FOOD_COMMAND_NAME##NB_ROTATIONS
const char* LCD_INIT_ROW_COMMAND_NAME = "I";// LCD_INIT_ROW_COMMAND_NAME##ROWNB
const char* LCD_BACKLIGHT_COMMAND_NAME = "K";
const char* LCD_CLEAR_COMMAND_NAME = "L";// LCD_CLEAR_COMMAND_NAME##
const char* LCD_BLINK_COMMAND_NAME = "N";
const char* LCD_APPEND_TEXT_COMMAND_NAME = "T";// LCD_APPEND_TEXT_COMMAND_NAME##Text
const char* LCD_APPEND_TEXT_VIA_MSGNB_COMMAND_NAME = "M";// LCD_APPEND_TEXT_VIA_MSGNB_COMMAND_NAME##MSGNB

#define Light1_RedBlue16W_Device_OnSlave_Index 1
#define Light2_Red20W_Device_OnSlave_Index 2
#define Light3_White12W_Device_OnSlave_Index 3
#define Light4_White15W_Device_OnSlave_Index 4
#define Light5_White15W_Device_OnSlave_Index 5
#define Light6_White12W_Device_OnSlave_Index 6
#define Light_Fan_Device_OnSlave_Index 7
#define CO2_Device_OnSlave_Index 8
#define Buzzer_Device_OnSlave_Index 9
#define Chassis_Fan_Device_OnSlave_Index 10


const byte emptyName =0;
const byte RBLightName =emptyName+1;
const byte RLightName  = RBLightName+1;
const byte W1LightName  = RLightName+1;
const byte W2LightName  = W1LightName+1;
const byte W3LightName  = W2LightName+1;
const byte W4LightName  = W3LightName+1;
const byte LightFanName  = W4LightName+1;
const byte Co2LightName  = LightFanName+1;
const byte MacroNutTextName  = Co2LightName+1;
const byte MicroNutTextName  = MacroNutTextName+1;
const byte HeaterTextName  = MicroNutTextName+1;
const byte FilterTextName  = HeaterTextName+1;
const byte AquariumWaterOutTextName  = FilterTextName+1;
const byte AquariumWaterInTextName  = AquariumWaterOutTextName+1;
const byte ReservoirWaterInTextName  = AquariumWaterInTextName+1;
const byte ChassisFanName  = ReservoirWaterInTextName+1;
const byte  roomTempSensorName = ChassisFanName+1;
const byte  lightTempSensorName = roomTempSensorName+1;
const byte  tankTempSensorName = lightTempSensorName+1;
const byte  chassisTempSensorName = tankTempSensorName+1;
const byte  waterLevelSensorName = chassisTempSensorName+1;
const byte  reservLevelSensorName = waterLevelSensorName+1;
const byte  dayTextName = reservLevelSensorName+1;
const byte  modeTextName = dayTextName+1;
const byte  setTextName = modeTextName+1;
const byte  closestTextName = setTextName+1;
const byte  maxTextName = closestTextName+1;
const byte  critTempTextName = maxTextName+1;
const byte  dateTextName = critTempTextName+1;
const byte  dateTimeTextName = dateTextName+1;
const byte  devicesTextName = dateTimeTextName+1;
const byte  timeTextNameSpaceAfterTime = devicesTextName+1;
const byte  wrongDigitTextName = timeTextNameSpaceAfterTime+1;
const byte  backOkTextName = wrongDigitTextName+1;
const byte  settingTextName = backOkTextName+1;
const byte  sensorBasedTextName = settingTextName+1;
const byte  anyLightTextName = sensorBasedTextName+1;
const byte  timerBasedTextName = anyLightTextName+1;
const byte  dailyTextName = timerBasedTextName+1;
const byte  MWFTextName = dailyTextName+1;
const byte  TuThSaTextName = MWFTextName+1;
const byte  okToSettingTextName = TuThSaTextName+1;
const byte  mlTextName = okToSettingTextName+1;
const byte  turnsTextName = mlTextName+1;
const byte  okToSaveTextName = turnsTextName+1;
const byte  errorTextName = okToSaveTextName+1;
const byte  waterOut = errorTextName+1;
const byte  waterChange = waterOut+1;
const byte  stopWaterChange = waterChange+1;
const byte  askIfWaterChange = stopWaterChange+1;
const byte  askIfPipIsKeptInDrain=askIfWaterChange+1;
const byte waterChangeTextName = askIfPipIsKeptInDrain+1;
const byte waterDrainingTxtName = waterChangeTextName+1;
const byte waterFillingTxtName = waterDrainingTxtName+1;
const byte  degCTextName = waterFillingTxtName+1;
const byte  wrongDateTimeError = degCTextName+1;
const byte  OkLevelTextName = wrongDateTimeError+1;
const byte  cmTxtName = OkLevelTextName+1;
const byte  idealLevelTxtName = cmTxtName+1;
const byte currentTxtName = idealLevelTxtName+1;
const byte expectedTxtName = currentTxtName+1;
const byte reservoirFillingTxtName = expectedTxtName+1;
const byte lowReservoirLevelTxtName = reservoirFillingTxtName+1;
const byte aquariumHeatingTxtName = lowReservoirLevelTxtName+1;
const byte timeTextNameNoSpaceAfterTime = aquariumHeatingTxtName+1;
const byte dispensingTxtName = timeTextNameNoSpaceAfterTime+1;
const byte FoodTextName  = dispensingTxtName+1;
const byte uptoTimeBasedTextName = FoodTextName+1;
const byte MTWTFTextName = uptoTimeBasedTextName+1;
const byte SaSuTextName = MTWTFTextName+1;
const byte allLightsOnTextName = SaSuTextName+1;
const byte durationForUptoTIme = allLightsOnTextName+1;
const byte uptoTimeTextName = durationForUptoTIme+1;
const byte oneLightTextName = uptoTimeTextName+1;
const byte solenoidCoolingTextName = oneLightTextName+1;
const byte waterBelowLevelTextName = solenoidCoolingTextName+1;
