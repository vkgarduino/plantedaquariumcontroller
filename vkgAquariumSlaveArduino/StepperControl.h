#include <Stepper.h>

const int stepsPerRevolution = 32;  // change this to fit the number of steps per revolution
Stepper stepper( stepsPerRevolution,STEPPER_PIN_A, STEPPER_PIN_C, STEPPER_PIN_B, STEPPER_PIN_D);
