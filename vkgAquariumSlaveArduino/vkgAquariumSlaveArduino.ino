#include "SlaveConstants.h"
#include "Utils.h"
#include "LcdControl.h"



#include "MasterCommand.h"
#include "SlaveOnlyConstant.h"
#include "StepperControl.h"
#include "Wire.h"



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
    for(int type1Command=1; type1Command<=NB_DEVICES_IN_SLAVE; type1Command++)
  {
     pinMode(COMMAND_TYPE1_PIN_INDENTIFIER[type1Command-1],OUTPUT);
     digitalWrite(COMMAND_TYPE1_PIN_INDENTIFIER[type1Command-1],RELAY_OFF);
  }

   InitializeLcd();

   stepper.setSpeed(200);//rpm
   lcd.begin();
}



void loop() {
  // put your main code here, to run repeatedly:
   MasterCommand command = ScanNextCommand();
   
   //Serial.print("command found: \"");
   //Serial.print(command.GetCommandName());
   //Serial.print("\" ,value: \"");
   //Serial.print(command.GetCommandValue_AsText());
   //Serial.println("\"");

   bool wrongMsg =false;
   if(command.IsName(LCD_DEVICE_CONTROL_COMMAND_NAME))
   {
         byte commandNb = command.GetCommandValue_AsInt(1);
         if( commandNb >0 && commandNb <= NB_DEVICES_IN_SLAVE)
         {
           byte pin_Nb = COMMAND_TYPE1_PIN_INDENTIFIER[commandNb-1];
           if(command.IsCommnadValue_AsTxt(2,CommandOn))
           {
               if(digitalRead(pin_Nb) == RELAY_OFF)
              {
                #ifdef ShowSlaveActionMsgs
                Serial.print(command.GetCommandName());
                Serial.print(" on.");
               #endif
               digitalWrite(pin_Nb,RELAY_ON);
               }
           }
           else
           {
           if(digitalRead(pin_Nb) == RELAY_ON)
           {
            #ifdef ShowSlaveActionMsgs
             Serial.print(command.GetCommandName());
             Serial.print(" off.");
            #endif
            digitalWrite(pin_Nb,RELAY_OFF);
           }
          }
         }
   }
   else if(command.IsName(LCD_BACKLIGHT_COMMAND_NAME) )
   {
       if(command.IsCommnadValue_AsTxt(1,CommandOn))
       {
            #ifdef ShowSlaveActionMsgs
            Serial.println("lcd on");
            #endif
            LcdGetUp();
       }
        else 
        {
            #ifdef ShowSlaveActionMsgs
            Serial.println("lcd off");
            #endif
            LcdSleep();
        }
   }
   else if(command.IsName(LCD_BLINK_COMMAND_NAME) )
   {
       if(command.IsCommnadValue_AsTxt(1,CommandOn))
       {
            #ifdef ShowSlaveActionMsgs
            Serial.println("lcd blinking");
            #endif
            SetLcdBlink(true);
       }
       else
       {
            #ifdef ShowSlaveActionMsgs
            Serial.println("lcd blinking off");
            #endif
            SetLcdBlink(false);
       }
   }
   else if(command.IsName(LCD_CLEAR_COMMAND_NAME) )
   {
       InitBothRow();
   }
   else if(command.IsName(LCD_INIT_ROW_COMMAND_NAME) )
   {
       int rowNb = command.GetCommandValue_AsInt(1);
       #ifdef ShowSlaveActionMsgs
         Serial.print("Init row number");
         Serial.println(rowNb);
        #endif
       if(rowNb >= 0 && rowNb < LCD_NB_ROWS)
          InitLcdRow(rowNb);
        else
          wrongMsg =true;
   }
   else if(command.IsName(LCD_SET_CURSOR_COMMAND_NAME) )
   {
       int columnNb = command.GetCommandValue_AsInt(1);
       int rowNb = command.GetCommandValue_AsInt(2);
       if( (columnNb >= 0 && columnNb < LCD_NB_COLUMNS) &&  (rowNb >= 0 && rowNb < LCD_NB_ROWS))
       {
            #ifdef ShowSlaveActionMsgs
          Serial.print("Cursor : ");
          Serial.print(columnNb);
          Serial.print(",");
          Serial.println(rowNb);
          #endif
          SetLcdCursor(columnNb,rowNb);
       }       
        else
          wrongMsg =true;
   }
   else if(command.IsName(LCD_APPEND_TEXT_COMMAND_NAME) )
   {
     const char * Msg = command.GetCommandValue_AsText();
      #ifdef ShowSlaveActionMsgs
       Serial.print("Printing \"");
      #endif

     if(Msg)
     {
           AppendTextInLcd(Msg);
          #ifdef ShowSlaveActionMsgs
            Serial.print(Msg);
          #endif
     }
     
     #ifdef ShowSlaveActionMsgs
       Serial.println("\" in Lcd.");
     #endif
   }
   else if(command.IsName(LCD_APPEND_TEXT_VIA_MSGNB_COMMAND_NAME) )
   {
       int msgNb = command.GetCommandValue_AsInt(1);
       const char * decodedMsg = DecodeTextMessage(msgNb);
       #ifdef ShowSlaveActionMsgs
         Serial.print("Printing \"");
        #endif
      if(decodedMsg)
      {
           AppendTextInLcd(decodedMsg);
           #ifdef ShowSlaveActionMsgs
            Serial.print(decodedMsg);
           #endif
      }
      #ifdef ShowSlaveActionMsgs
       Serial.println("\" in Lcd.");
      #endif
   }
   else if(command.IsName(FOOD_COMMAND_NAME) )
   {
       int nbRotations = command.GetCommandValue_AsInt(1);
       if(nbRotations != 0 )
       {
          stepper.step(2048*nbRotations);
       }       
        else
          wrongMsg =true;
   }
   else
   {
   }

}
