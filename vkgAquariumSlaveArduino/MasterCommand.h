#include "string.h"

class MasterCommand
{
  String commandName;
  String commandValue;

  String TxtFromValue(byte iValueNb)
  {
       String txt1="",txt2="";
       if(!BreakStringGivenRelimiter(commandValue, MASTER_COMMAND_RELIMITER, &txt1,&txt2))
          txt1 = commandValue;

       if(1 == iValueNb) return txt1;
       if(2 == iValueNb) return txt2;
       return "";
  }
  public:
     MasterCommand()
     {
     }

     void SetCommandName(const char* iStr)
     {
       commandName = iStr;
     }

     bool IsName(const char* iStr)
     {
      if(AreSame(GetCommandName(),iStr))
        return true;
      return false;
     }

  
     void SetCommandValue(const char* iStr)
     {
       commandValue = iStr;
     }
     const char* GetCommandName()
     {
       return commandName.c_str();
     }

     const char* GetCommandValue_AsText()
     {
       return commandValue.c_str();
     }
     
     
     int GetCommandValue_AsInt(byte iValNb)
     {
       return TxtFromValue(iValNb).toInt();
     }

     bool IsCommnadValue_AsTxt(byte iValNb, const char* iStr)
     {
       if(TxtFromValue(iValNb) == iStr)
          return true;
       return false;
     }
};

#define BeingReadState 1
#define AlreadyReadState 2

byte masterCommandPrefixState = BeingReadState;
String masterCommandReadPrefix = "";
String masterCommandReadContent = "";


void InitReadCommand()
{
   masterCommandPrefixState = BeingReadState;
   masterCommandReadPrefix = "";
   masterCommandReadContent = "";
}


String readContent = ""; 

bool ExtractCommandFromContent(MasterCommand *oMasterCommand)
{
  if(masterCommandReadContent.length() < 1)
     return false;

  String commandName, commandValue;
  
  if(!BreakStringGivenRelimiter(masterCommandReadContent,MASTER_COMMAND_RELIMITER,&commandName,&commandValue))
  {
     return false;
  }
  if(0 == commandName.length())
  {
     return false;
  }

  oMasterCommand->SetCommandName(commandName.c_str());
  oMasterCommand->SetCommandValue(commandValue.c_str());
  return true;
}

MasterCommand ScanNextCommand()
{

  String MASTER_COMMAND_PREFIXstr = MASTER_COMMAND_PREFIX;
  String MASTER_COMMAND_POSTFIXstr = MASTER_COMMAND_POSTFIX;
  String MASTER_COMMAND_RELIMITERstr = MASTER_COMMAND_RELIMITER;
  
  while(1)
  {
      while(!Serial.available());

      while (Serial.available() > 0) 
      {
         int incomingByte = Serial.read();
         if(-1 == incomingByte)
         {
           continue;
         } 
         char incomingChar = (char) incomingByte;
         //Serial.print(incomingChar);
         if (incomingChar == '\0')
            continue;
         if (incomingChar == '\n')
            continue;

         if(masterCommandPrefixState == BeingReadState)
         {
            if (incomingChar == ' ')
               continue;
            String tmp = masterCommandReadPrefix + incomingChar;
            int length = tmp.length();
            if(tmp.length() > MASTER_COMMAND_PREFIXstr.length())
            {
               InitReadCommand();
              continue;
            }

            String sub = MASTER_COMMAND_PREFIXstr.substring(0,tmp.length());
            if(tmp != sub)
            {
               InitReadCommand();
               if(tmp.length() > 1)
               {
                  tmp = tmp.substring(1,tmp.length());
                  sub = MASTER_COMMAND_PREFIXstr.substring(0,tmp.length());
                  if(tmp != sub)
                     continue;
               }
               else
                 continue;
            }

            if(tmp.length() == MASTER_COMMAND_PREFIXstr.length())
            {
               masterCommandPrefixState = AlreadyReadState;
               masterCommandReadPrefix = "";
               continue;
            }
            else
            {
              masterCommandReadPrefix = tmp;
            }
         }
         else
         {
            String tmp = masterCommandReadContent + incomingChar;
            if(tmp.length() >= MASTER_COMMAND_POSTFIXstr.length())
            {
               String sub = tmp.substring(tmp.length()-MASTER_COMMAND_POSTFIXstr.length(), tmp.length());//read last characters (same as nb of char in postfix)
               if(MASTER_COMMAND_POSTFIXstr == sub)
               {
                  if(tmp.length() == MASTER_COMMAND_POSTFIXstr.length())
                  {
                     InitReadCommand();
                     continue;
                  }
                  masterCommandReadContent = tmp.substring(0, tmp.length()-MASTER_COMMAND_POSTFIXstr.length());
                  MasterCommand mc;
                  if(!ExtractCommandFromContent(&mc))
                  {
                     InitReadCommand();
                     continue;
                  }
                  InitReadCommand();
                  return mc;
               }
            }
            masterCommandReadContent = tmp;
         }
      }
  }
  MasterCommand emptyCmd;
  return emptyCmd;
}
