bool BreakStringGivenRelimiter(String iString, String iRelimiter, String *oString1, String *oString2)
{
  *oString1 = "";
  *oString2 = "";

  int index = iString.indexOf(iRelimiter);
  if(-1 == index)
     return false;

  if(index > 0)
    *oString1 = iString.substring(0,index);
  
  *oString2 = iString.substring(index+iRelimiter.length(),iString.length());

  return true;
}

bool AreSame(const char * iName1, const char * iName2)
{
  if(0 == strcmp(iName1,iName2))
    return true;
  return false;
}

int GetCurrentSecs()
{
   return millis()/1000;
}

byte GetValueFromBits(byte * source,byte minBitIndexOfSource, byte maxBitIndexOfSource)
{
  byte value=0;
  for(byte i = minBitIndexOfSource; i<= maxBitIndexOfSource; i++)
  {
    bitWrite(value,i-minBitIndexOfSource,bitRead(*source,i));
  }
  return value;
}

void SetBitsInTarget(byte * target, byte iValue, byte minBitIndexOfTarget, byte maxBitIndexOfTarget)
{
  for(byte i = minBitIndexOfTarget; i<= maxBitIndexOfTarget; i++)
  {
    bitWrite(*target,i,bitRead(iValue,i-minBitIndexOfTarget));
  }
}


bool GetBoolValueFromBit(byte * source,byte bitIndex)
{
     if(GetValueFromBits(source,bitIndex,bitIndex) == 1)
       return true;
     else
       return false;
}

void SetBoolValueInBit(byte * target,bool iValue , byte bitIndex)
{
    if(iValue)
      SetBitsInTarget(target,1, bitIndex,bitIndex);
    else
      SetBitsInTarget(target,0, bitIndex,bitIndex);
}

byte GetDigitNbFromByte(int iValue, byte NbTotalBytes, byte iDigitNb)
{
   // 27    2 is digit#1,  7 is digit#2
   // 1345    1 is digit#1,  5 is digit#4
  return (byte)(((int)(iValue / pow(10,(NbTotalBytes-iDigitNb) ) ) ) % 10);
}
int GetIntFromDigit(byte iDigitNb1, byte iDigitNb2, byte iDigitNb3, byte iDigitNb4)
{
   // 27    2 is digit#1,  7 is digit#2
   // 1345    1 is digit#1,  5 is digit#4
   return ((int)iDigitNb1)*1000 + ((int)iDigitNb2)*100 + ((int)iDigitNb3)*10 + (int)iDigitNb4;
}
